﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;


namespace aspVidly.Utils
{
    public class NLogger
    {
        public static NLog.ILogger Il = NLog.LogManager.GetCurrentClassLogger();

        public static void I(string info)
        {
            Il.Info(info);
            
        }

        public static void D(string info)
        {
            Il.Debug(info);

        }
        public static void W(string info)
        {
            Il.Warn(info);

        }
        public static void E(string info)
        {
            Il.Error(info);

        }
        public static void F(string info)
        {
            Il.Fatal(info);

        }

    }



}