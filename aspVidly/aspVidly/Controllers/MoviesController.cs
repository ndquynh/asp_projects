﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using aspVidly.Models;
using aspVidly.Utils;
using aspVidly.ViewModels;

namespace aspVidly.Controllers
{
    public class MoviesController : Controller
    {

        public MoviesController()
        {
            Debug.Listeners.Add(new TextWriterTraceListener("D:\\temp\\test.txt"));
            Debug.AutoFlush = true;
            Debug.WriteLine("test");

            

        }

        // GET: Movies/Random
        public ActionResult Random()
        {
            var movie = new Movie() {Name = "Shrek!"};
            //return View(movie);
            //return Content("Hello world");
            //return HttpNotFound("PAGE NOT FOUND");
            //return new EmptyResult();
            //return RedirectToAction("Index", "Home", new { page=1, name="love" });

            ViewData["Movie"] = movie;
            ViewBag.Movie = movie;

            //var viewResult = new ViewResult {ViewData = {Model = movie}};
            return View(movie) as ViewResult;           
        }

        public ActionResult Transaction()
        {
           
            var movie = new Movie(){Name = "Alice in the Wonderland"};

            var customers = new List<Customer>()
            {
                new Customer {Name = "John"},
                new Customer() {Name = "Alice"},
                new Customer {Name = "Cusomer Random"},

            };

            var randomMovieVM = new RandomMovieViewModel()
            {
                Movie = movie,
                Customers = customers
                
            };

            return View(randomMovieVM) as ViewResult;
        }
            



        public ActionResult Edit(int movieid)
        {       
            NLogger.D("movieid = " + movieid);

            return Content("id=" + movieid);
        }

        public ActionResult Index(int? pageIndex, string sortBy)
        {
            if (!pageIndex.HasValue)
            {
                pageIndex = 1;
            }

            if (String.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "Name";
            }

            return View(new Movie(){ Name = "Alice in the Wonderland"}) as ViewResult;
        }

        public ActionResult ByReleaseDate(int year, int month)
        {
            return Content(String.Format("year={0}&month={1}", year, month));
        }

        [Route("movies/pop/{year:length(4):max(2018)}/{month:regex(\\d{2}):range(1, 12)}")]
        public ActionResult ByPopYear(int year, int month)
        {
            return View(String.Format("{0}/{1}", year, month));
        }


    }
}