﻿namespace QLSV
{
    partial class FormQLSinhVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormQLSinhVien));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cbxLop = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridViewSV = new System.Windows.Forms.DataGridView();
            this.tool_btnAdd = new System.Windows.Forms.ToolStripButton();
            this.tool_btnEdit = new System.Windows.Forms.ToolStripButton();
            this.tool_btnRemove = new System.Windows.Forms.ToolStripButton();
            this.tool_btnSave = new System.Windows.Forms.ToolStripButton();
            this.tool_btnClose = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSV)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tool_btnAdd,
            this.tool_btnEdit,
            this.tool_btnRemove,
            this.tool_btnSave,
            this.tool_btnClose});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(595, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // cbxLop
            // 
            this.cbxLop.FormattingEnabled = true;
            this.cbxLop.Location = new System.Drawing.Point(101, 37);
            this.cbxLop.Name = "cbxLop";
            this.cbxLop.Size = new System.Drawing.Size(155, 21);
            this.cbxLop.TabIndex = 1;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(364, 33);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(101, 85);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(155, 20);
            this.textBox1.TabIndex = 3;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(364, 85);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(200, 20);
            this.textBox2.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Lop";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Ma Sinh Vien";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(284, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ten Sinh Vien";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(284, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Ngay Sinh";
            // 
            // dataGridViewSV
            // 
            this.dataGridViewSV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSV.Location = new System.Drawing.Point(16, 127);
            this.dataGridViewSV.Name = "dataGridViewSV";
            this.dataGridViewSV.Size = new System.Drawing.Size(567, 170);
            this.dataGridViewSV.TabIndex = 9;
            // 
            // tool_btnAdd
            // 
            this.tool_btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("tool_btnAdd.Image")));
            this.tool_btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_btnAdd.Name = "tool_btnAdd";
            this.tool_btnAdd.Size = new System.Drawing.Size(49, 22);
            this.tool_btnAdd.Text = "Add";
            this.tool_btnAdd.Click += new System.EventHandler(this.tool_btnAdd_Click);
            // 
            // tool_btnEdit
            // 
            this.tool_btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("tool_btnEdit.Image")));
            this.tool_btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_btnEdit.Name = "tool_btnEdit";
            this.tool_btnEdit.Size = new System.Drawing.Size(47, 22);
            this.tool_btnEdit.Text = "Edit";
            this.tool_btnEdit.Click += new System.EventHandler(this.tool_btnEdit_Click);
            // 
            // tool_btnRemove
            // 
            this.tool_btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("tool_btnRemove.Image")));
            this.tool_btnRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_btnRemove.Name = "tool_btnRemove";
            this.tool_btnRemove.Size = new System.Drawing.Size(70, 22);
            this.tool_btnRemove.Text = "Remove";
            this.tool_btnRemove.Click += new System.EventHandler(this.tool_btnRemove_Click);
            // 
            // tool_btnSave
            // 
            this.tool_btnSave.Image = ((System.Drawing.Image)(resources.GetObject("tool_btnSave.Image")));
            this.tool_btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_btnSave.Name = "tool_btnSave";
            this.tool_btnSave.Size = new System.Drawing.Size(51, 22);
            this.tool_btnSave.Text = "Save";
            this.tool_btnSave.Click += new System.EventHandler(this.tool_btnSave_Click);
            // 
            // tool_btnClose
            // 
            this.tool_btnClose.Image = ((System.Drawing.Image)(resources.GetObject("tool_btnClose.Image")));
            this.tool_btnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_btnClose.Name = "tool_btnClose";
            this.tool_btnClose.Size = new System.Drawing.Size(56, 22);
            this.tool_btnClose.Text = "Close";
            this.tool_btnClose.Click += new System.EventHandler(this.tool_btnClose_Click);
            // 
            // FormQLSinhVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 309);
            this.Controls.Add(this.dataGridViewSV);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.cbxLop);
            this.Controls.Add(this.toolStrip1);
            this.Name = "FormQLSinhVien";
            this.Text = "Quan Ly Sinh Vien";
            this.Load += new System.EventHandler(this.FormQLSinhVien_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ComboBox cbxLop;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridViewSV;
        private System.Windows.Forms.ToolStripButton tool_btnAdd;
        private System.Windows.Forms.ToolStripButton tool_btnEdit;
        private System.Windows.Forms.ToolStripButton tool_btnRemove;
        private System.Windows.Forms.ToolStripButton tool_btnSave;
        private System.Windows.Forms.ToolStripButton tool_btnClose;
    }
}