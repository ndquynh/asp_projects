﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSV
{
    public partial class FormQuanLyKhoa : Form
    {
        QLSVDataContext qlsv = new QLSVDataContext();

        public FormQuanLyKhoa()
        {
            InitializeComponent();
        }

        private void FormQuanLyKhoa_Load(object sender, EventArgs e)
        {
            LoadKhoaData();
            tool_btnAdd.Enabled = false;
            tool_btnEdit.Enabled = false;
            tool_btnSave.Enabled = false;
            tool_btnRemove.Enabled = false;
        }

        private void LoadKhoaData()
        {
            var res = from khoa in qlsv.Khoas select khoa;
            dataGridViewKhoa.DataSource = res;

            tbxMaKhoa.Text = "";
            tbxTenKhoa.Text = "";
        }

        private void tool_btnAdd_Click(object sender, EventArgs e)
        {
            Khoa kh = new Khoa();
            kh.MaKhoa = tbxMaKhoa.Text;
            kh.TenKhoa = tbxTenKhoa.Text;
            
            qlsv.Khoas.InsertOnSubmit(kh);
            qlsv.SubmitChanges();
            LoadKhoaData();
        }

        private void tool_btnEdit_Click(object sender, EventArgs e)
        {
            if (dataGridViewKhoa.CurrentRow == null) return;
            
            string maKhoa = dataGridViewKhoa.CurrentRow.Cells[0].Value.ToString();
            Khoa khoa = qlsv.Khoas.Where(t => t.MaKhoa == maKhoa).FirstOrDefault();

            tbxMaKhoa.Text = khoa.MaKhoa;
            tbxTenKhoa.Text = khoa.TenKhoa;

            tool_btnAdd.Enabled = false;
            tool_btnEdit.Enabled = false;
            tool_btnRemove.Enabled = false;
            tool_btnSave.Enabled = true;
            if (tbxMaKhoa.Enabled)
                tbxMaKhoa.Enabled = false;
        }

        private void tool_btnRemove_Click(object sender, EventArgs e)
        {
            if (dataGridViewKhoa.CurrentRow == null) return;
            string maKhoa = dataGridViewKhoa.CurrentRow.Cells[0].Value.ToString();
            
            Khoa khoa = qlsv.Khoas.Where(t => t.MaKhoa == maKhoa).FirstOrDefault();
            qlsv.Khoas.DeleteOnSubmit(khoa);
            qlsv.SubmitChanges();
            
            LoadKhoaData();
        }

        private void tool_btnSave_Click(object sender, EventArgs e)
        {
            if(!tbxMaKhoa.Enabled) 
                tbxMaKhoa.Enabled = true;
            else
            {
                return;
            }
            tool_btnAdd.Enabled = true;
            tool_btnEdit.Enabled = true;
            tool_btnRemove.Enabled = true;
            tool_btnSave.Enabled = false;

            Khoa kh = qlsv.Khoas.Where(t => t.MaKhoa == tbxMaKhoa.Text).FirstOrDefault();
            kh.TenKhoa = tbxTenKhoa.Text;
            
            qlsv.SubmitChanges();

            LoadKhoaData();
        }

        private void tool_btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tbxMaKhoa_TextChanged(object sender, EventArgs e)
        {

            if (CheckAddCreteria())
            {
                tool_btnAdd.Enabled = true;
            }
            else
            {
                tool_btnAdd.Enabled = false;
            }
        }

        private Boolean CheckAddCreteria()
        {
            if (tbxMaKhoa.Text.Trim().Count() == 0 || tbxTenKhoa.Text.Trim().Count() == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void tbxTenKhoa_TextChanged(object sender, EventArgs e)
        {
            if (CheckAddCreteria())
            {
                tool_btnAdd.Enabled = true;
            }
            else
            {
                tool_btnAdd.Enabled = false;
            }
        }

        private void dataGridViewKhoa_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //tool_btnAdd.Enabled = false;
            tool_btnEdit.Enabled = true;
            tool_btnSave.Enabled = false;
            tool_btnRemove.Enabled = true;
        }

        
    }
}

