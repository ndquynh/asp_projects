﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFormWithDB
{
    public partial class FormThemNguoiDungVaoNhom : Form
    {
        public FormThemNguoiDungVaoNhom()
        {
            InitializeComponent();
        }

        private void qL_NguoiDungBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.qL_NguoiDungBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSet1);

        }

        private void FormThemNguoiDungVaoNhom_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.QL_NhomNguoiDung' table. You can move, or remove it, as needed.
            this.qL_NhomNguoiDungTableAdapter.Fill(this.dataSet1.QL_NhomNguoiDung);
            // TODO: This line of code loads data into the 'dataSet1.QL_NguoiDung' table. You can move, or remove it, as needed.
            this.qL_NguoiDungTableAdapter.Fill(this.dataSet1.QL_NguoiDung);

        }


        private void qL_NhomNguoiDungComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.themNguoiDungVaoNhomTableAdapter.Fill_my(this.dataSet1.ThemNguoiDungVaoNhom, qL_NhomNguoiDungComboBox.SelectedValue.ToString());
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void qL_NguoiDungDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string userName = qL_NguoiDungDataGridView.SelectedCells[0].Value.ToString();
            string maNhom = qL_NhomNguoiDungComboBox.SelectedValue.ToString();

            //check user exist

            //themNguoiDungVaoNhomBindingSource.Insert()
            themNguoiDungVaoNhomTableAdapter.InsertQuery_QL_NguoiDungNhomNguoiDung(userName, maNhom, "guadsad");

                        
            themNguoiDungVaoNhomDataGridView.Update();
            themNguoiDungVaoNhomDataGridView.Refresh();          

        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            string userName = themNguoiDungVaoNhomDataGridView.SelectedCells[0].Value.ToString();
            string maNhom = themNguoiDungVaoNhomDataGridView.SelectedCells[1].Value.ToString();

            //check user exist

            //themNguoiDungVaoNhomBindingSource.Insert()
            themNguoiDungVaoNhomTableAdapter.DeleteQuery_NguoiDungNhomNguoiDung(userName, maNhom);


            themNguoiDungVaoNhomDataGridView.Update();
            themNguoiDungVaoNhomDataGridView.Refresh();          
        }
    }
}
