﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class DatabaseManager
    {
        public static DatabaseManager I = new DatabaseManager();

        public MyConnection Connection { get; set; }

        private SqlConnection sqlConn;

        private DatabaseManager()
        {
        }
 
        public void initialize(MyConnection conn)
        {
            I.Connection = conn;
            String connectionString = String.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}",
                                Connection.ServerName, Connection.DatabaseName, Connection.UserName, Connection.Password);
            sqlConn = new SqlConnection(connectionString);
        }

        public int executeQuery(String query)
        {
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            SqlCommand cmd = new SqlCommand(query, sqlConn);
            //Goi ExecuteNonQuery de gui command
            int val = cmd.ExecuteNonQuery();
            //Kiem tra ket noi truoc khi dong
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }

            return val;
        }

        public int insert(String query)
        {
            return executeQuery(query);
        }

        public int delete(String query)
        {
            return executeQuery(query);
        }

        public int update(String query)
        {
            return executeQuery(query);
        }

    }
}
