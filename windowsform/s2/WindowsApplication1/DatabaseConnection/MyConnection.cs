﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class MyConnection
    {

        public String ServerName { get; set; }

        public String DatabaseName { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }


        public MyConnection(String serverName, String DbName, String user, String password)
        {
            ServerName = serverName;
            DatabaseName = DbName;
            UserName = user;
            Password = password;
        }

    }
}
