USE [dbQLTB]
GO
/****** Object:  Table [dbo].[tblNhanVien]    Script Date: 16/10/2018 8:12:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblNhanVien](
	[MaNV] [varchar](12) NOT NULL,
	[TenNV] [nvarchar](50) NULL,
	[ChucVu] [nvarchar](50) NULL,
	[MaPhong] [varchar](12) NULL,
 CONSTRAINT [PK_tblNhanVien] PRIMARY KEY CLUSTERED 
(
	[MaNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblPhieuMuon]    Script Date: 16/10/2018 8:12:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPhieuMuon](
	[NVLapPhieu] [varchar](12) NOT NULL,
	[MaTB] [varchar](12) NOT NULL,
	[TinhTrang] [nvarchar](50) NULL,
	[NgayMuon] [datetime] NULL,
	[NVMuon] [varchar](12) NULL,
	[SoLuong] [int] NULL,
	[MaPhong] [varchar](12) NULL,
 CONSTRAINT [PK_tblPhieuMuon] PRIMARY KEY CLUSTERED 
(
	[NVLapPhieu] ASC,
	[MaTB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblPhieuNhap]    Script Date: 16/10/2018 8:12:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPhieuNhap](
	[MaPN] [varchar](12) NOT NULL,
	[MaNV] [varchar](12) NULL,
	[MaTB] [varchar](12) NULL,
	[NgayNhap] [datetime] NULL,
	[SoLuong] [int] NULL,
	[TinhTrang] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblPhieuNhap] PRIMARY KEY CLUSTERED 
(
	[MaPN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblPhieuTra]    Script Date: 16/10/2018 8:12:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPhieuTra](
	[MaTB] [varchar](12) NOT NULL,
	[NVTra] [varchar](12) NOT NULL,
	[NVNhan] [varchar](12) NULL,
	[NgayTra] [datetime] NULL,
	[SoLuong] [int] NULL,
	[TinhTrang] [nvarchar](50) NULL,
	[MaPhong] [varchar](12) NULL,
 CONSTRAINT [PK_tblPhieuTra] PRIMARY KEY CLUSTERED 
(
	[MaTB] ASC,
	[NVTra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblPhong]    Script Date: 16/10/2018 8:12:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPhong](
	[MaPhong] [varchar](12) NOT NULL,
	[TenPhong] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblPhong] PRIMARY KEY CLUSTERED 
(
	[MaPhong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblThietBi]    Script Date: 16/10/2018 8:12:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblThietBi](
	[MaTB] [varchar](12) NOT NULL,
	[TenTB] [nvarchar](50) NULL,
	[TinhTrang] [nvarchar](50) NULL,
	[SoLuong] [int] NULL,
	[GhiChu] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblThietBi] PRIMARY KEY CLUSTERED 
(
	[MaTB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tblNhanVien] ([MaNV], [TenNV], [ChucVu], [MaPhong]) VALUES (N'1', N'An', N'NhanVien', N'1')
INSERT [dbo].[tblNhanVien] ([MaNV], [TenNV], [ChucVu], [MaPhong]) VALUES (N'2', N'Binh', N'NhanVien', N'4')
INSERT [dbo].[tblNhanVien] ([MaNV], [TenNV], [ChucVu], [MaPhong]) VALUES (N'3', N'Tai', N'NhanVien', N'3')
INSERT [dbo].[tblNhanVien] ([MaNV], [TenNV], [ChucVu], [MaPhong]) VALUES (N'4', N'Hai', N'TruongPhong', N'2')
INSERT [dbo].[tblNhanVien] ([MaNV], [TenNV], [ChucVu], [MaPhong]) VALUES (N'5', N'Kim', N'NhanVien', N'6')
INSERT [dbo].[tblNhanVien] ([MaNV], [TenNV], [ChucVu], [MaPhong]) VALUES (N'6', N'Ly', N'NhanVien', N'5')
INSERT [dbo].[tblPhieuMuon] ([NVLapPhieu], [MaTB], [TinhTrang], [NgayMuon], [NVMuon], [SoLuong], [MaPhong]) VALUES (N'1', N'1', N'moi', CAST(0x00009E0B00000000 AS DateTime), N'2', 2, N'4')
INSERT [dbo].[tblPhieuMuon] ([NVLapPhieu], [MaTB], [TinhTrang], [NgayMuon], [NVMuon], [SoLuong], [MaPhong]) VALUES (N'1', N'4', N'moi', CAST(0x0000A97500000000 AS DateTime), N'2', 5, N'6')
INSERT [dbo].[tblPhieuMuon] ([NVLapPhieu], [MaTB], [TinhTrang], [NgayMuon], [NVMuon], [SoLuong], [MaPhong]) VALUES (N'1', N'5', N'moi', CAST(0x0000A91500000000 AS DateTime), N'4', 2, N'2')
INSERT [dbo].[tblPhieuMuon] ([NVLapPhieu], [MaTB], [TinhTrang], [NgayMuon], [NVMuon], [SoLuong], [MaPhong]) VALUES (N'1', N'6', N'moi', CAST(0x0000A80E00000000 AS DateTime), N'6', 2, N'5')
INSERT [dbo].[tblPhieuNhap] ([MaPN], [MaNV], [MaTB], [NgayNhap], [SoLuong], [TinhTrang]) VALUES (N'1', N'1', N'1', CAST(0x00009C9E00000000 AS DateTime), 2, N'Moi')
INSERT [dbo].[tblPhieuNhap] ([MaPN], [MaNV], [MaTB], [NgayNhap], [SoLuong], [TinhTrang]) VALUES (N'2', N'1', N'2', CAST(0x00009C9E00000000 AS DateTime), 4, N'Moi')
INSERT [dbo].[tblPhieuNhap] ([MaPN], [MaNV], [MaTB], [NgayNhap], [SoLuong], [TinhTrang]) VALUES (N'3', N'1', N'3', CAST(0x00009C9E00000000 AS DateTime), 1, N'Cu')
INSERT [dbo].[tblPhieuNhap] ([MaPN], [MaNV], [MaTB], [NgayNhap], [SoLuong], [TinhTrang]) VALUES (N'4', N'1', N'4', CAST(0x00009C9E00000000 AS DateTime), 4, N'Moi')
INSERT [dbo].[tblPhieuNhap] ([MaPN], [MaNV], [MaTB], [NgayNhap], [SoLuong], [TinhTrang]) VALUES (N'5', N'1', N'5', CAST(0x00009C9E00000000 AS DateTime), 20, N'Moi')
INSERT [dbo].[tblPhieuNhap] ([MaPN], [MaNV], [MaTB], [NgayNhap], [SoLuong], [TinhTrang]) VALUES (N'6', N'1', N'6', CAST(0x00009C9E00000000 AS DateTime), 40, N'Moi')
INSERT [dbo].[tblPhieuNhap] ([MaPN], [MaNV], [MaTB], [NgayNhap], [SoLuong], [TinhTrang]) VALUES (N'7', N'1', N'7', CAST(0x00009C9E00000000 AS DateTime), 50, N'moi')
INSERT [dbo].[tblPhieuTra] ([MaTB], [NVTra], [NVNhan], [NgayTra], [SoLuong], [TinhTrang], [MaPhong]) VALUES (N'1', N'2', N'1', CAST(0x0000A97B00000000 AS DateTime), 2, N'da su dung ', N'4')
INSERT [dbo].[tblPhong] ([MaPhong], [TenPhong]) VALUES (N'1', N'KeToan')
INSERT [dbo].[tblPhong] ([MaPhong], [TenPhong]) VALUES (N'2', N'KyThuat')
INSERT [dbo].[tblPhong] ([MaPhong], [TenPhong]) VALUES (N'3', N'Thu Mua')
INSERT [dbo].[tblPhong] ([MaPhong], [TenPhong]) VALUES (N'4', N'Marketing')
INSERT [dbo].[tblPhong] ([MaPhong], [TenPhong]) VALUES (N'5', N'KinhDoanh')
INSERT [dbo].[tblPhong] ([MaPhong], [TenPhong]) VALUES (N'6', N'NhanSu')
INSERT [dbo].[tblThietBi] ([MaTB], [TenTB], [TinhTrang], [SoLuong], [GhiChu]) VALUES (N'1', N'BiaHoSo', N'Moi', 2, NULL)
INSERT [dbo].[tblThietBi] ([MaTB], [TenTB], [TinhTrang], [SoLuong], [GhiChu]) VALUES (N'2', N'BiaKep', N'Moi', 4, NULL)
INSERT [dbo].[tblThietBi] ([MaTB], [TenTB], [TinhTrang], [SoLuong], [GhiChu]) VALUES (N'3', N'MayChieu', N'Cu', 1, NULL)
INSERT [dbo].[tblThietBi] ([MaTB], [TenTB], [TinhTrang], [SoLuong], [GhiChu]) VALUES (N'4', N'ManHinh', N'Moi', 4, N'')
INSERT [dbo].[tblThietBi] ([MaTB], [TenTB], [TinhTrang], [SoLuong], [GhiChu]) VALUES (N'5', N'CPU', N'Moi', 20, NULL)
INSERT [dbo].[tblThietBi] ([MaTB], [TenTB], [TinhTrang], [SoLuong], [GhiChu]) VALUES (N'6', N'BanPhim', N'Moi', 40, NULL)
INSERT [dbo].[tblThietBi] ([MaTB], [TenTB], [TinhTrang], [SoLuong], [GhiChu]) VALUES (N'7', N'Chuot', N'Moi', 50, NULL)
ALTER TABLE [dbo].[tblNhanVien]  WITH CHECK ADD  CONSTRAINT [FK_tblNhanVien_tblPhong] FOREIGN KEY([MaPhong])
REFERENCES [dbo].[tblPhong] ([MaPhong])
GO
ALTER TABLE [dbo].[tblNhanVien] CHECK CONSTRAINT [FK_tblNhanVien_tblPhong]
GO
ALTER TABLE [dbo].[tblPhieuMuon]  WITH CHECK ADD  CONSTRAINT [FK_tblPhieuMuon_tblNhanVien] FOREIGN KEY([NVLapPhieu])
REFERENCES [dbo].[tblNhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[tblPhieuMuon] CHECK CONSTRAINT [FK_tblPhieuMuon_tblNhanVien]
GO
ALTER TABLE [dbo].[tblPhieuMuon]  WITH CHECK ADD  CONSTRAINT [FK_tblPhieuMuon_tblPhong] FOREIGN KEY([MaPhong])
REFERENCES [dbo].[tblPhong] ([MaPhong])
GO
ALTER TABLE [dbo].[tblPhieuMuon] CHECK CONSTRAINT [FK_tblPhieuMuon_tblPhong]
GO
ALTER TABLE [dbo].[tblPhieuMuon]  WITH CHECK ADD  CONSTRAINT [FK_tblPhieuMuon_tblThietBi] FOREIGN KEY([MaTB])
REFERENCES [dbo].[tblThietBi] ([MaTB])
GO
ALTER TABLE [dbo].[tblPhieuMuon] CHECK CONSTRAINT [FK_tblPhieuMuon_tblThietBi]
GO
ALTER TABLE [dbo].[tblPhieuNhap]  WITH CHECK ADD  CONSTRAINT [FK_tblPhieuNhap_tblNhanVien] FOREIGN KEY([MaNV])
REFERENCES [dbo].[tblNhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[tblPhieuNhap] CHECK CONSTRAINT [FK_tblPhieuNhap_tblNhanVien]
GO
ALTER TABLE [dbo].[tblPhieuNhap]  WITH CHECK ADD  CONSTRAINT [FK_tblPhieuNhap_tblThietBi] FOREIGN KEY([MaTB])
REFERENCES [dbo].[tblThietBi] ([MaTB])
GO
ALTER TABLE [dbo].[tblPhieuNhap] CHECK CONSTRAINT [FK_tblPhieuNhap_tblThietBi]
GO
ALTER TABLE [dbo].[tblPhieuTra]  WITH CHECK ADD  CONSTRAINT [FK_tblPhieuTra_tblNhanVien] FOREIGN KEY([NVTra])
REFERENCES [dbo].[tblNhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[tblPhieuTra] CHECK CONSTRAINT [FK_tblPhieuTra_tblNhanVien]
GO
ALTER TABLE [dbo].[tblPhieuTra]  WITH CHECK ADD  CONSTRAINT [FK_tblPhieuTra_tblNhanVien1] FOREIGN KEY([NVNhan])
REFERENCES [dbo].[tblNhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[tblPhieuTra] CHECK CONSTRAINT [FK_tblPhieuTra_tblNhanVien1]
GO
ALTER TABLE [dbo].[tblPhieuTra]  WITH CHECK ADD  CONSTRAINT [FK_tblPhieuTra_tblPhong] FOREIGN KEY([MaPhong])
REFERENCES [dbo].[tblPhong] ([MaPhong])
GO
ALTER TABLE [dbo].[tblPhieuTra] CHECK CONSTRAINT [FK_tblPhieuTra_tblPhong]
GO
ALTER TABLE [dbo].[tblPhieuTra]  WITH CHECK ADD  CONSTRAINT [FK_tblPhieuTra_tblThietBi] FOREIGN KEY([MaTB])
REFERENCES [dbo].[tblThietBi] ([MaTB])
GO
ALTER TABLE [dbo].[tblPhieuTra] CHECK CONSTRAINT [FK_tblPhieuTra_tblThietBi]
GO
