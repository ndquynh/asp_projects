﻿namespace WindowsFormsApplication1
{
    partial class FormMuonThietBi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtsoluong = new System.Windows.Forms.TextBox();
            this.cbbmatb = new System.Windows.Forms.ComboBox();
            this.cbbtinhtrangtb = new System.Windows.Forms.ComboBox();
            this.cbbmaphong = new System.Windows.Forms.ComboBox();
            this.btnlapphieu = new System.Windows.Forms.Button();
            this.btnxoa = new System.Windows.Forms.Button();
            this.btnthoat = new System.Windows.Forms.Button();
            this.dgwChiTietPhieuMuon = new System.Windows.Forms.DataGridView();
            this.dtpngaymuon = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbbnhanviennhan = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbbNvLapPhieu = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgwChiTietPhieuMuon)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(325, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Phiếu Mượn Thiết Bị";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "NV Lập Phiếu:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Mã Thiết Bị:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tình Trạng TB:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(357, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Nhân Viên Nhận:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(357, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Mã Phòng:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(357, 67);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Số Lượng:";
            // 
            // txtsoluong
            // 
            this.txtsoluong.Location = new System.Drawing.Point(456, 64);
            this.txtsoluong.Name = "txtsoluong";
            this.txtsoluong.Size = new System.Drawing.Size(193, 20);
            this.txtsoluong.TabIndex = 11;
            // 
            // cbbmatb
            // 
            this.cbbmatb.FormattingEnabled = true;
            this.cbbmatb.Location = new System.Drawing.Point(104, 59);
            this.cbbmatb.Name = "cbbmatb";
            this.cbbmatb.Size = new System.Drawing.Size(198, 21);
            this.cbbmatb.TabIndex = 12;
            // 
            // cbbtinhtrangtb
            // 
            this.cbbtinhtrangtb.FormattingEnabled = true;
            this.cbbtinhtrangtb.Location = new System.Drawing.Point(104, 86);
            this.cbbtinhtrangtb.Name = "cbbtinhtrangtb";
            this.cbbtinhtrangtb.Size = new System.Drawing.Size(198, 21);
            this.cbbtinhtrangtb.TabIndex = 13;
            // 
            // cbbmaphong
            // 
            this.cbbmaphong.FormattingEnabled = true;
            this.cbbmaphong.Location = new System.Drawing.Point(456, 100);
            this.cbbmaphong.Name = "cbbmaphong";
            this.cbbmaphong.Size = new System.Drawing.Size(193, 21);
            this.cbbmaphong.TabIndex = 15;
            // 
            // btnlapphieu
            // 
            this.btnlapphieu.Location = new System.Drawing.Point(100, 44);
            this.btnlapphieu.Name = "btnlapphieu";
            this.btnlapphieu.Size = new System.Drawing.Size(96, 23);
            this.btnlapphieu.TabIndex = 16;
            this.btnlapphieu.Text = "Mượn Thiết Bị";
            this.btnlapphieu.UseVisualStyleBackColor = true;
            this.btnlapphieu.Click += new System.EventHandler(this.btnlapphieu_Click);
            // 
            // btnxoa
            // 
            this.btnxoa.Location = new System.Drawing.Point(287, 44);
            this.btnxoa.Name = "btnxoa";
            this.btnxoa.Size = new System.Drawing.Size(120, 23);
            this.btnxoa.TabIndex = 18;
            this.btnxoa.Text = "Xóa";
            this.btnxoa.UseVisualStyleBackColor = true;
            this.btnxoa.Click += new System.EventHandler(this.btnxoa_Click);
            // 
            // btnthoat
            // 
            this.btnthoat.Location = new System.Drawing.Point(490, 44);
            this.btnthoat.Name = "btnthoat";
            this.btnthoat.Size = new System.Drawing.Size(108, 23);
            this.btnthoat.TabIndex = 19;
            this.btnthoat.Text = "Thoát";
            this.btnthoat.UseVisualStyleBackColor = true;
            this.btnthoat.Click += new System.EventHandler(this.btnthoat_Click);
            // 
            // dgwChiTietPhieuMuon
            // 
            this.dgwChiTietPhieuMuon.AllowUserToAddRows = false;
            this.dgwChiTietPhieuMuon.AllowUserToDeleteRows = false;
            this.dgwChiTietPhieuMuon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwChiTietPhieuMuon.Location = new System.Drawing.Point(12, 308);
            this.dgwChiTietPhieuMuon.Name = "dgwChiTietPhieuMuon";
            this.dgwChiTietPhieuMuon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgwChiTietPhieuMuon.Size = new System.Drawing.Size(719, 224);
            this.dgwChiTietPhieuMuon.TabIndex = 20;
            // 
            // dtpngaymuon
            // 
            this.dtpngaymuon.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpngaymuon.Location = new System.Drawing.Point(104, 113);
            this.dtpngaymuon.Name = "dtpngaymuon";
            this.dtpngaymuon.Size = new System.Drawing.Size(198, 20);
            this.dtpngaymuon.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbbNvLapPhieu);
            this.groupBox1.Controls.Add(this.cbbnhanviennhan);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbbmaphong);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtsoluong);
            this.groupBox1.Controls.Add(this.dtpngaymuon);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbbtinhtrangtb);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cbbmatb);
            this.groupBox1.Location = new System.Drawing.Point(39, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(668, 143);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thuộc Tính";
            // 
            // cbbnhanviennhan
            // 
            this.cbbnhanviennhan.FormattingEnabled = true;
            this.cbbnhanviennhan.Location = new System.Drawing.Point(456, 36);
            this.cbbnhanviennhan.Name = "cbbnhanviennhan";
            this.cbbnhanviennhan.Size = new System.Drawing.Size(193, 21);
            this.cbbnhanviennhan.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Ngày Mượn:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnlapphieu);
            this.groupBox2.Controls.Add(this.btnthoat);
            this.groupBox2.Controls.Add(this.btnxoa);
            this.groupBox2.Location = new System.Drawing.Point(12, 199);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(719, 94);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chức Năng";
            // 
            // cbbNvLapPhieu
            // 
            this.cbbNvLapPhieu.FormattingEnabled = true;
            this.cbbNvLapPhieu.Location = new System.Drawing.Point(104, 30);
            this.cbbNvLapPhieu.Name = "cbbNvLapPhieu";
            this.cbbNvLapPhieu.Size = new System.Drawing.Size(198, 21);
            this.cbbNvLapPhieu.TabIndex = 26;
            // 
            // FormMuonThietBi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 544);
            this.Controls.Add(this.dgwChiTietPhieuMuon);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "FormMuonThietBi";
            this.Text = "FormPhieuNhapThietBi";
            this.Load += new System.EventHandler(this.FormMuonThietBi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgwChiTietPhieuMuon)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtsoluong;
        private System.Windows.Forms.ComboBox cbbmatb;
        private System.Windows.Forms.ComboBox cbbtinhtrangtb;
        private System.Windows.Forms.ComboBox cbbmaphong;
        private System.Windows.Forms.Button btnlapphieu;
        private System.Windows.Forms.Button btnxoa;
        private System.Windows.Forms.Button btnthoat;
        private System.Windows.Forms.DataGridView dgwChiTietPhieuMuon;
        private System.Windows.Forms.DateTimePicker dtpngaymuon;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbbnhanviennhan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbbNvLapPhieu;
    }
}