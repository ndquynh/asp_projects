﻿namespace WindowsFormsApplication1
{
    partial class FormTraThietBi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtsoluong = new System.Windows.Forms.TextBox();
            this.cbbnhanvientra = new System.Windows.Forms.ComboBox();
            this.cbbnhanviennhap = new System.Windows.Forms.ComboBox();
            this.cbbmaphong = new System.Windows.Forms.ComboBox();
            this.btnlapphieu = new System.Windows.Forms.Button();
            this.btnxoa = new System.Windows.Forms.Button();
            this.btnthoat = new System.Windows.Forms.Button();
            this.dgwchitietphieunhap = new System.Windows.Forms.DataGridView();
            this.dtpngaytra = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbbtinhtrang = new System.Windows.Forms.ComboBox();
            this.cbbmathietbi = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgwchitietphieunhap)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(322, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Phiếu Trả Thiết Bị ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Mã Thiết Bị";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nhân Viên Trả:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Nhân Viên Nhận:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(270, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Số Lượng:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(270, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Mã Phòng:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(270, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Tình Trạng:";
            // 
            // txtsoluong
            // 
            this.txtsoluong.Location = new System.Drawing.Point(343, 38);
            this.txtsoluong.Name = "txtsoluong";
            this.txtsoluong.Size = new System.Drawing.Size(121, 20);
            this.txtsoluong.TabIndex = 11;
            // 
            // cbbnhanvientra
            // 
            this.cbbnhanvientra.FormattingEnabled = true;
            this.cbbnhanvientra.Location = new System.Drawing.Point(103, 61);
            this.cbbnhanvientra.Name = "cbbnhanvientra";
            this.cbbnhanvientra.Size = new System.Drawing.Size(121, 21);
            this.cbbnhanvientra.TabIndex = 12;
            // 
            // cbbnhanviennhap
            // 
            this.cbbnhanviennhap.FormattingEnabled = true;
            this.cbbnhanviennhap.Location = new System.Drawing.Point(104, 88);
            this.cbbnhanviennhap.Name = "cbbnhanviennhap";
            this.cbbnhanviennhap.Size = new System.Drawing.Size(121, 21);
            this.cbbnhanviennhap.TabIndex = 13;
            // 
            // cbbmaphong
            // 
            this.cbbmaphong.FormattingEnabled = true;
            this.cbbmaphong.Location = new System.Drawing.Point(343, 97);
            this.cbbmaphong.Name = "cbbmaphong";
            this.cbbmaphong.Size = new System.Drawing.Size(121, 21);
            this.cbbmaphong.TabIndex = 15;
            // 
            // btnlapphieu
            // 
            this.btnlapphieu.Location = new System.Drawing.Point(33, 43);
            this.btnlapphieu.Name = "btnlapphieu";
            this.btnlapphieu.Size = new System.Drawing.Size(75, 23);
            this.btnlapphieu.TabIndex = 16;
            this.btnlapphieu.Text = "Lập Phiếu";
            this.btnlapphieu.UseVisualStyleBackColor = true;
            // 
            // btnxoa
            // 
            this.btnxoa.Location = new System.Drawing.Point(217, 43);
            this.btnxoa.Name = "btnxoa";
            this.btnxoa.Size = new System.Drawing.Size(75, 23);
            this.btnxoa.TabIndex = 18;
            this.btnxoa.Text = "Xóa";
            this.btnxoa.UseVisualStyleBackColor = true;
            this.btnxoa.Click += new System.EventHandler(this.btnxoa_Click);
            // 
            // btnthoat
            // 
            this.btnthoat.Location = new System.Drawing.Point(386, 43);
            this.btnthoat.Name = "btnthoat";
            this.btnthoat.Size = new System.Drawing.Size(75, 23);
            this.btnthoat.TabIndex = 19;
            this.btnthoat.Text = "Thoát";
            this.btnthoat.UseVisualStyleBackColor = true;
            this.btnthoat.Click += new System.EventHandler(this.btnthoat_Click);
            // 
            // dgwchitietphieunhap
            // 
            this.dgwchitietphieunhap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwchitietphieunhap.Location = new System.Drawing.Point(39, 330);
            this.dgwchitietphieunhap.Name = "dgwchitietphieunhap";
            this.dgwchitietphieunhap.Size = new System.Drawing.Size(651, 213);
            this.dgwchitietphieunhap.TabIndex = 20;
            // 
            // dtpngaytra
            // 
            this.dtpngaytra.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpngaytra.Location = new System.Drawing.Point(101, 115);
            this.dtpngaytra.Name = "dtpngaytra";
            this.dtpngaytra.Size = new System.Drawing.Size(121, 20);
            this.dtpngaytra.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbbtinhtrang);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cbbmathietbi);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbbmaphong);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtsoluong);
            this.groupBox1.Controls.Add(this.dtpngaytra);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbbnhanviennhap);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cbbnhanvientra);
            this.groupBox1.Location = new System.Drawing.Point(119, 51);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(479, 143);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Danh Sách";
            // 
            // cbbtinhtrang
            // 
            this.cbbtinhtrang.FormattingEnabled = true;
            this.cbbtinhtrang.Location = new System.Drawing.Point(343, 67);
            this.cbbtinhtrang.Name = "cbbtinhtrang";
            this.cbbtinhtrang.Size = new System.Drawing.Size(121, 21);
            this.cbbtinhtrang.TabIndex = 26;
            // 
            // cbbmathietbi
            // 
            this.cbbmathietbi.FormattingEnabled = true;
            this.cbbmathietbi.Location = new System.Drawing.Point(101, 28);
            this.cbbmathietbi.Name = "cbbmathietbi";
            this.cbbmathietbi.Size = new System.Drawing.Size(121, 21);
            this.cbbmathietbi.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Ngày Trả:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnlapphieu);
            this.groupBox2.Controls.Add(this.btnthoat);
            this.groupBox2.Controls.Add(this.btnxoa);
            this.groupBox2.Location = new System.Drawing.Point(108, 221);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(499, 103);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chức Năng";
            // 
            // FormTraThietBi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 586);
            this.Controls.Add(this.dgwchitietphieunhap);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "FormTraThietBi";
            this.Text = "FormPhieuNhapThietBi";
            this.Load += new System.EventHandler(this.FormTraThietBi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgwchitietphieunhap)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtsoluong;
        private System.Windows.Forms.ComboBox cbbnhanvientra;
        private System.Windows.Forms.ComboBox cbbnhanviennhap;
        private System.Windows.Forms.ComboBox cbbmaphong;
        private System.Windows.Forms.Button btnlapphieu;
        private System.Windows.Forms.Button btnxoa;
        private System.Windows.Forms.Button btnthoat;
        private System.Windows.Forms.DataGridView dgwchitietphieunhap;
        private System.Windows.Forms.DateTimePicker dtpngaytra;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbbmathietbi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbbtinhtrang;
    }
}