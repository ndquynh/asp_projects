﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FormMuonThietBi : Form
    {
        private QLTBDataContext _dataContext = new QLTBDataContext();

        public FormMuonThietBi()
        {
            InitializeComponent();
        }

        private void label9_Click(object sender, EventArgs e)
        {
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FormMuonThietBi_Load(object sender, EventArgs e)
        {
            LayDuLieuThietBi();

            LayDuLieuNhanVien();

            LayDuLieuPhongs();

            txtsoluong.Text = "0";

            cbbtinhtrangtb.DataSource = new string[] {"Moi", "Cu"};

            LoadDanhSachMuon();
        }

        private void LoadDanhSachMuon()
        {
            dgwChiTietPhieuMuon.DataSource = Muons();
        }

        private bool KiemTraDuLieuDauVao(string maNvChoMuon, string maNvMuon, string maTb, DateTime ngayNhap,
            string soLuong, string tinhtrang, bool isEdit)
        {
            if (!(from nv in _dataContext.tblNhanViens where maNvChoMuon == nv.MaNV select nv).Any())
            {
                HienLoi(String.Format("Không tồn tại nhân viên có mã {0}", maNvChoMuon));
                return false;
            }

            if (!(from nv in _dataContext.tblNhanViens where maNvMuon == nv.MaNV select nv).Any())
            {
                HienLoi(String.Format("Không tồn tại nhân viên có mã {0}", maNvMuon));
                return false;
            }

            tblThietBi thietBi = ThietBis().FirstOrDefault(tb => maTb == tb.MaTB);

            if (thietBi == null)
            {
                HienLoi(String.Format("Không tồn tại thiết bị có mã {0}", maTb));
                return false;
            }


            if (Muons().Any(i => maNvChoMuon == i.NVLapPhieu && maTb == i.MaTB))
            {
                HienLoi(
                    String.Format("Nhân viên \"{0}\" chỉ có thể cho mượn thiết bị \"{1}\" 1 lần", maNvChoMuon, maTb));
                return false;
            }

            int sl = 0;
            try
            {
                sl = Int32.Parse(soLuong);
            }
            catch (Exception e)
            {
                HienLoi(String.Format("Số lượng {0} không hợp lệ", soLuong));
                return false;
            }

            if (sl <= 0)
            {
                HienLoi("Số lượng phải > 0");
                return false;
            }

            if (sl > thietBi.SoLuong)
            {
                HienLoi(String.Format("Số lượng khả dụng của thiết bị \"{1}\" hiện tại là \"{0}\". Vui lòng điều chỉnh.", thietBi.SoLuong, thietBi.MaTB));
                return false;
            }

            return true;
        }

        private void LayDuLieuThietBi()
        {
            cbbmatb.DataSource = ThietBis().Select(tb => tb.MaTB).ToList();
        }

        private void LayDuLieuNhanVien()
        {
            cbbNvLapPhieu.DataSource = NhanViens().Select(i => i.MaNV).ToList();
            cbbnhanviennhan.DataSource = NhanViens().Select(i => i.MaNV).ToList();
        }

        private void LayDuLieuPhongs()
        {
            cbbmaphong.DataSource = Phongs().Select(p => p.MaPhong).ToList();
        }

        private IQueryable<tblNhanVien> NhanViens()
        {
            return (from i in _dataContext.tblNhanViens select i);
        }

        private IQueryable<tblThietBi> ThietBis()
        {
            return (from tb in _dataContext.tblThietBis select tb);
        }

        private IQueryable<tblPhong> Phongs()
        {
            return (from i in _dataContext.tblPhongs select i);
        }

        private IQueryable<tblPhieuMuon> Muons()
        {
            return (from i in _dataContext.tblPhieuMuons select i);
        }

        private void btnlapphieu_Click(object sender, EventArgs e)
        {
            if (!YesNo("Bạn có muốn thêm thông tin vào trong cơ sở dữ liệu không?")) return;


            string maNvLap = cbbNvLapPhieu.SelectedValue.ToString();
            string maNvMuon = cbbnhanviennhan.SelectedValue.ToString();
            string maTB = cbbmatb.SelectedValue.ToString();
            DateTime ngayNhap = dtpngaymuon.Value;
            string soLuong = txtsoluong.Text;
            string tinhtrang = cbbtinhtrangtb.SelectedValue.ToString();
            string phong = cbbmaphong.SelectedValue.ToString();

            if (!KiemTraDuLieuDauVao(maNvLap, maNvMuon, maTB, ngayNhap, soLuong, tinhtrang, false))
            {
                return;
            }

            ;

            _dataContext.tblPhieuMuons.InsertOnSubmit(
                new tblPhieuMuon()
                {
                    NVLapPhieu = maNvLap,
                    NVMuon = maNvMuon,
                    MaTB = maTB,
                    NgayMuon = ngayNhap,
                    SoLuong = Int32.Parse(soLuong),
                    TinhTrang = tinhtrang,
                    MaPhong = phong
                });

            CapNhatSoLuongThietBi(maTB, 0, Int32.Parse(soLuong));

            _dataContext.SubmitChanges();

            LoadDanhSachMuon();
            LayDuLieuThietBi();
        }

        private void CapNhatSoLuongThietBi(string maTb, int soLuongCu, int soLuongMoi)
        {
            if (_dataContext.tblThietBis != null)
            {
                var tbb = ThietBis().FirstOrDefault(tb => maTb == tb.MaTB);
                tbb.SoLuong += soLuongCu;
                tbb.SoLuong -= soLuongMoi;

                _dataContext.SubmitChanges();
            }
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            if (dgwChiTietPhieuMuon.SelectedRows.Count == 0) return;

            string maNvChoMuon = dgwChiTietPhieuMuon.SelectedRows[0].Cells[0].Value.ToString();
            string maTb = dgwChiTietPhieuMuon.SelectedRows[0].Cells[1].Value.ToString();

            if (YesNo(String.Format(
                "BẠn có muốn xóa phiếu mượn có mã nhân viên \"{0}\" và thiết bị có mã \"{1}\" không?", maNvChoMuon,
                maTb)))
            {
                tblPhieuMuon firstOrDefault =
                    Muons().FirstOrDefault(i => maNvChoMuon == i.NVLapPhieu && maTb == i.MaTB);
                if (firstOrDefault != null)
                {
                    _dataContext.tblPhieuMuons.DeleteOnSubmit(firstOrDefault);
                    _dataContext.SubmitChanges();

                    LoadDanhSachMuon();
                }
            }
        }

        private void HienLoi(string error)
        {
            MessageBox.Show(error, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private bool YesNo(string msg)
        {
            return MessageBox.Show(msg, "Chú ý!", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                       MessageBoxDefaultButton.Button1) == DialogResult.Yes;
        }
    }
}