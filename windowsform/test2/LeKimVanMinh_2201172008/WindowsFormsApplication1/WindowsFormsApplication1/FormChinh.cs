﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FormChinh : Form
    {
        public FormChinh()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnnhapthietbi_Click(object sender, EventArgs e)
        {
            new FormPhieuNhapThietBi().ShowDialog();
        }

        private void btnmuonthietbi_Click(object sender, EventArgs e)
        {
            new FormMuonThietBi().ShowDialog();
        }

        private void btntrathietbi_Click(object sender, EventArgs e)
        {
            new FormTraThietBi().ShowDialog();
        }
    }
}
