﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FormTraThietBi : Form
    {

        private QLTBDataContext _dataContext = new QLTBDataContext();

        public FormTraThietBi()
        {
            InitializeComponent();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {

        }

        private void FormTraThietBi_Load(object sender, EventArgs e)
        {

            cbbmathietbi.DataSource = ThietBis().Select(i => i.MaTB).ToList();
            cbbnhanvientra.DataSource = NhanViens().Select(i => i.MaNV).ToList();
            cbbnhanviennhap.DataSource = NhanViens().Select(i => i.MaNV).ToList();
            cbbmaphong.DataSource = Phongs().Select(i => i.MaPhong).ToList();
            cbbtinhtrang.DataSource = new string[] {"Moi", "Cu"};

            LoadDanhSachTra();

            txtsoluong.Text = "0";
        }

        private void LoadDanhSachTra()
        {
            dgwchitietphieunhap.DataSource = Tras().Select(i=> new {i.MaTB, i.NVTra, i.NVNhan, i.NgayTra, i.SoLuong, i.TinhTrang, i.MaPhong} ).ToList();
        }


        private IQueryable<tblNhanVien> NhanViens()
        {
            return (from i in _dataContext.tblNhanViens select i);
        }

        private IQueryable<tblThietBi> ThietBis()
        {
            return (from tb in _dataContext.tblThietBis select tb);
        }

        private IQueryable<tblPhong> Phongs()
        {
            return (from i in _dataContext.tblPhongs select i);
        }

        private IQueryable<tblPhieuMuon> Muons()
        {
            return (from i in _dataContext.tblPhieuMuons select i);
        }

        private IQueryable<tblPhieuTra> Tras()
        {
            return (from i in _dataContext.tblPhieuTras select i);
        }
    }
}
