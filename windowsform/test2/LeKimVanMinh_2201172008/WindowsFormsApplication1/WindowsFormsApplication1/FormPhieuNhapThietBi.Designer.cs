﻿namespace WindowsFormsApplication1
{
    partial class FormPhieuNhapThietBi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnlapphieu = new System.Windows.Forms.Button();
            this.btnchinhsua = new System.Windows.Forms.Button();
            this.btnxoa = new System.Windows.Forms.Button();
            this.btnthoat = new System.Windows.Forms.Button();
            this.dgwchitietphieunhap = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtpngaynhap = new System.Windows.Forms.DateTimePicker();
            this.cbbtinhtrang = new System.Windows.Forms.ComboBox();
            this.cbbmatb = new System.Windows.Forms.ComboBox();
            this.cbbmanv = new System.Windows.Forms.ComboBox();
            this.txtsoluong = new System.Windows.Forms.TextBox();
            this.txtmapn = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgwchitietphieunhap)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(315, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Phiếu Nhập Thiệt Bị";
            // 
            // btnlapphieu
            // 
            this.btnlapphieu.Location = new System.Drawing.Point(29, 33);
            this.btnlapphieu.Name = "btnlapphieu";
            this.btnlapphieu.Size = new System.Drawing.Size(75, 23);
            this.btnlapphieu.TabIndex = 16;
            this.btnlapphieu.Text = "Lập Phiếu";
            this.btnlapphieu.UseVisualStyleBackColor = true;
            this.btnlapphieu.Click += new System.EventHandler(this.btnlapphieu_Click);
            // 
            // btnchinhsua
            // 
            this.btnchinhsua.Location = new System.Drawing.Point(142, 33);
            this.btnchinhsua.Name = "btnchinhsua";
            this.btnchinhsua.Size = new System.Drawing.Size(75, 23);
            this.btnchinhsua.TabIndex = 17;
            this.btnchinhsua.Text = "Chỉnh Sửa";
            this.btnchinhsua.UseVisualStyleBackColor = true;
            this.btnchinhsua.Click += new System.EventHandler(this.btnchinhsua_Click);
            // 
            // btnxoa
            // 
            this.btnxoa.Location = new System.Drawing.Point(261, 33);
            this.btnxoa.Name = "btnxoa";
            this.btnxoa.Size = new System.Drawing.Size(75, 23);
            this.btnxoa.TabIndex = 18;
            this.btnxoa.Text = "Xóa";
            this.btnxoa.UseVisualStyleBackColor = true;
            this.btnxoa.Click += new System.EventHandler(this.btnxoa_Click);
            // 
            // btnthoat
            // 
            this.btnthoat.Location = new System.Drawing.Point(382, 33);
            this.btnthoat.Name = "btnthoat";
            this.btnthoat.Size = new System.Drawing.Size(75, 23);
            this.btnthoat.TabIndex = 19;
            this.btnthoat.Text = "Thoát";
            this.btnthoat.UseVisualStyleBackColor = true;
            this.btnthoat.Click += new System.EventHandler(this.btnthoat_Click);
            // 
            // dgwchitietphieunhap
            // 
            this.dgwchitietphieunhap.AllowUserToAddRows = false;
            this.dgwchitietphieunhap.AllowUserToDeleteRows = false;
            this.dgwchitietphieunhap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwchitietphieunhap.Location = new System.Drawing.Point(55, 330);
            this.dgwchitietphieunhap.MultiSelect = false;
            this.dgwchitietphieunhap.Name = "dgwchitietphieunhap";
            this.dgwchitietphieunhap.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgwchitietphieunhap.Size = new System.Drawing.Size(634, 213);
            this.dgwchitietphieunhap.TabIndex = 20;
            this.dgwchitietphieunhap.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgwchitietphieunhap_RowStateChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtpngaynhap);
            this.groupBox1.Controls.Add(this.cbbtinhtrang);
            this.groupBox1.Controls.Add(this.cbbmatb);
            this.groupBox1.Controls.Add(this.cbbmanv);
            this.groupBox1.Controls.Add(this.txtsoluong);
            this.groupBox1.Controls.Add(this.txtmapn);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(151, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(434, 143);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thuộc Tính";
            // 
            // dtpngaynhap
            // 
            this.dtpngaynhap.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpngaynhap.Location = new System.Drawing.Point(294, 29);
            this.dtpngaynhap.Name = "dtpngaynhap";
            this.dtpngaynhap.Size = new System.Drawing.Size(121, 20);
            this.dtpngaynhap.TabIndex = 33;
            // 
            // cbbtinhtrang
            // 
            this.cbbtinhtrang.FormattingEnabled = true;
            this.cbbtinhtrang.Location = new System.Drawing.Point(294, 93);
            this.cbbtinhtrang.Name = "cbbtinhtrang";
            this.cbbtinhtrang.Size = new System.Drawing.Size(121, 21);
            this.cbbtinhtrang.TabIndex = 32;
            // 
            // cbbmatb
            // 
            this.cbbmatb.FormattingEnabled = true;
            this.cbbmatb.Location = new System.Drawing.Point(70, 93);
            this.cbbmatb.Name = "cbbmatb";
            this.cbbmatb.Size = new System.Drawing.Size(121, 21);
            this.cbbmatb.TabIndex = 31;
            // 
            // cbbmanv
            // 
            this.cbbmanv.FormattingEnabled = true;
            this.cbbmanv.Location = new System.Drawing.Point(70, 66);
            this.cbbmanv.Name = "cbbmanv";
            this.cbbmanv.Size = new System.Drawing.Size(121, 21);
            this.cbbmanv.TabIndex = 30;
            // 
            // txtsoluong
            // 
            this.txtsoluong.Location = new System.Drawing.Point(294, 63);
            this.txtsoluong.Name = "txtsoluong";
            this.txtsoluong.Size = new System.Drawing.Size(121, 20);
            this.txtsoluong.TabIndex = 29;
            // 
            // txtmapn
            // 
            this.txtmapn.Location = new System.Drawing.Point(70, 34);
            this.txtmapn.Name = "txtmapn";
            this.txtmapn.Size = new System.Drawing.Size(121, 20);
            this.txtmapn.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(232, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "Số Lượng:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(232, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Tình Trạng:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(232, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Ngày Nhập:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Mã TB:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Mã NV:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Mã PN:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnlapphieu);
            this.groupBox2.Controls.Add(this.btnchinhsua);
            this.groupBox2.Controls.Add(this.btnthoat);
            this.groupBox2.Controls.Add(this.btnxoa);
            this.groupBox2.Location = new System.Drawing.Point(128, 219);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(477, 83);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chức Năng";
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(46, 308);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(651, 244);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ChiTietPhieuNhap";
            // 
            // FormPhieuNhapThietBi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 586);
            this.Controls.Add(this.dgwchitietphieunhap);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Name = "FormPhieuNhapThietBi";
            this.Text = "FormPhieuNhapThietBi";
            this.Load += new System.EventHandler(this.FormPhieuNhapThietBi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgwchitietphieunhap)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnlapphieu;
        private System.Windows.Forms.Button btnchinhsua;
        private System.Windows.Forms.Button btnxoa;
        private System.Windows.Forms.Button btnthoat;
        private System.Windows.Forms.DataGridView dgwchitietphieunhap;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker dtpngaynhap;
        private System.Windows.Forms.ComboBox cbbtinhtrang;
        private System.Windows.Forms.ComboBox cbbmatb;
        private System.Windows.Forms.ComboBox cbbmanv;
        private System.Windows.Forms.TextBox txtsoluong;
        private System.Windows.Forms.TextBox txtmapn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}