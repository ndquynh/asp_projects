﻿namespace WindowsFormsApplication1
{
    partial class FormChinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnnhapthietbi = new System.Windows.Forms.Button();
            this.btnmuonthietbi = new System.Windows.Forms.Button();
            this.btntrathietbi = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnThoat = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(226, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chương Trình Quản Lý Thiết Bị Doanh Nghiệp";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnnhapthietbi
            // 
            this.btnnhapthietbi.Location = new System.Drawing.Point(95, 106);
            this.btnnhapthietbi.Name = "btnnhapthietbi";
            this.btnnhapthietbi.Size = new System.Drawing.Size(105, 59);
            this.btnnhapthietbi.TabIndex = 1;
            this.btnnhapthietbi.Text = "Nhập Thiết Bị";
            this.btnnhapthietbi.UseVisualStyleBackColor = true;
            this.btnnhapthietbi.Click += new System.EventHandler(this.btnnhapthietbi_Click);
            // 
            // btnmuonthietbi
            // 
            this.btnmuonthietbi.Location = new System.Drawing.Point(320, 104);
            this.btnmuonthietbi.Name = "btnmuonthietbi";
            this.btnmuonthietbi.Size = new System.Drawing.Size(108, 61);
            this.btnmuonthietbi.TabIndex = 2;
            this.btnmuonthietbi.Text = "Mượn Thiết Bị";
            this.btnmuonthietbi.UseVisualStyleBackColor = true;
            this.btnmuonthietbi.Click += new System.EventHandler(this.btnmuonthietbi_Click);
            // 
            // btntrathietbi
            // 
            this.btntrathietbi.Location = new System.Drawing.Point(577, 102);
            this.btntrathietbi.Name = "btntrathietbi";
            this.btntrathietbi.Size = new System.Drawing.Size(107, 61);
            this.btntrathietbi.TabIndex = 3;
            this.btntrathietbi.Text = "Trả Thiết Bị";
            this.btntrathietbi.UseVisualStyleBackColor = true;
            this.btntrathietbi.Click += new System.EventHandler(this.btntrathietbi_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Danh Sách Các Chức Năng:";
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(321, 241);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(107, 61);
            this.btnThoat.TabIndex = 5;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.button4_Click);
            // 
            // FormChinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 380);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btntrathietbi);
            this.Controls.Add(this.btnmuonthietbi);
            this.Controls.Add(this.btnnhapthietbi);
            this.Controls.Add(this.label1);
            this.Name = "FormChinh";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnnhapthietbi;
        private System.Windows.Forms.Button btnmuonthietbi;
        private System.Windows.Forms.Button btntrathietbi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnThoat;
    }
}

