﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FormPhieuNhapThietBi : Form
    {
        private QLTBDataContext _dataContext = new QLTBDataContext();

        public FormPhieuNhapThietBi()
        {
            InitializeComponent();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void HienLoi(string error)
        {
            MessageBox.Show(error, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private bool YesNo(string msg)
        {
            return MessageBox.Show(msg, "Chú ý!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes;
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            if (YesNo("BẠn có thực sự muốn thoát không?"))
            {
                Application.Exit();
            }
            
        }

        private void FormPhieuNhapThietBi_Load(object sender, EventArgs e)
        {
            LayDuLieuCacPhieuNhap();
            cbbmanv.DataSource = from nv in _dataContext.tblNhanViens select nv.MaNV;

            LayDuLieuThietBi();

            cbbtinhtrang.DataSource = new string[] {"Moi", "Cu"};
        }

        private void LayDuLieuThietBi()
        {
            cbbmatb.DataSource = from tb in _dataContext.tblThietBis select tb.MaTB;
        }

        private void LayDuLieuCacPhieuNhap()
        {
            dgwchitietphieunhap.DataSource = _dataContext.tblPhieuNhaps.Select(pntb => new { pntb.MaPN, pntb.MaNV,  pntb.MaTB, pntb.NgayNhap, pntb.SoLuong, pntb.TinhTrang }).ToList();

           // txtmapn.Text = TinhToanMaPhieuNhapKeTiep().ToString();
        }

        private int TinhToanMaPhieuNhapKeTiep()
        {
            if (!_dataContext.tblPhieuNhaps.Any())
            {
                return 1;
            }

            var max = _dataContext.tblPhieuNhaps.Max(pntb => Int32.Parse(pntb.MaPN.Trim()));

            return max + 1;
        }

        private void btnlapphieu_Click(object sender, EventArgs e)
        {
            if(!YesNo("Bạn có muốn thêm thông tin vào trong cơ sở dữ liệu không?")) return;

            string maPN = txtmapn.Text;
            string maNV = cbbmanv.SelectedValue.ToString();
            string maTB = cbbmatb.SelectedValue.ToString();
            DateTime ngayNhap = dtpngaynhap.Value;
            string soLuong = txtsoluong.Text;
            string tinhtrang = cbbtinhtrang.SelectedValue.ToString();


            if (!KiemTraDuLieuDauVao(maPN, maNV, maTB, ngayNhap, soLuong, tinhtrang, false))
            {
                return;
            };
            
            _dataContext.tblPhieuNhaps.InsertOnSubmit(new tblPhieuNhap(){MaPN = maPN,MaNV = maNV,  MaTB = maTB, NgayNhap = ngayNhap, SoLuong = Int32.Parse(soLuong), TinhTrang = tinhtrang});

            CapNhatSoLuongThietBi(maTB, 0, Int32.Parse(soLuong));

            _dataContext.SubmitChanges();

            LayDuLieuCacPhieuNhap();
            LayDuLieuThietBi();
        }

        private void CapNhatSoLuongThietBi(string maTb, int? soLuongCu, int soLuongMoi)
        {
            if (_dataContext.tblThietBis != null)
            {
                var tbb = _dataContext.tblThietBis.FirstOrDefault(tb => maTb == tb.MaTB);
                tbb.SoLuong -= soLuongCu;
                tbb.SoLuong += soLuongMoi;

                _dataContext.SubmitChanges();
            }
        }

        private bool KiemTraDuLieuDauVao(string maPn, string maNv, string maTb, DateTime ngayNhap, string soLuong, string tinhtrang, bool isEdit)
        {
            if (!isEdit)
            {
                var ketqua =
                    (from phieuNhap in _dataContext.tblPhieuNhaps where maPn == phieuNhap.MaPN select phieuNhap).Any();
                if (ketqua)
                {
                    HienLoi("Mã phiếu nhập đã tồn tại");
                    return false;
                }
            }

            if (!(from nv in _dataContext.tblNhanViens where maNv == nv.MaNV select nv).Any())
            {
                HienLoi(String.Format("Không tồn tại nhân viên có mã {0}", maNv));
                return false;
            }
            
            if (!(from tb in _dataContext.tblThietBis where maTb == tb.MaTB select tb).Any())
            {
                HienLoi(String.Format("Không tồn tại thiết bị có mã {0}", maTb));
                return false;
            }

            int sl = 0; 
            try
            {
                sl = Int32.Parse(soLuong);
            }
            catch (Exception e)
            {
                HienLoi(String.Format("Số lượng {0} không hợp lệ", soLuong));
                return false;
            }

            if (sl <= 0)
            {
                HienLoi("Số lượng phải > 0");
                return false;
            }

            return true;
        }


        private void btnchinhsua_Click(object sender, EventArgs e)
        {

            if (YesNo("Bạn có muốn lưu thông tin đã sửa không?"))
            {
                if (_firstOrDefault == null) return;

                //validate
                var maPn = txtmapn.Text;
                if (maPn != _firstOrDefault.MaPN)
                {
                    HienLoi("Không được phép thay đổi mã phiếu nhập");
                    return;
                }

                var maVn = cbbmanv.SelectedValue.ToString();
                var maTb = cbbmatb.SelectedValue.ToString();
                var ngayNhap = dtpngaynhap.Value;
                var sl = txtsoluong.Text.ToString();
                var tt = cbbtinhtrang.SelectedValue.ToString();

                if (!KiemTraDuLieuDauVao(maPn:maPn, maNv:maVn, maTb:maTb, ngayNhap:ngayNhap, soLuong:sl, tinhtrang:tt, isEdit:true)) return;

                tblPhieuNhap phieuNhap = _dataContext.tblPhieuNhaps.FirstOrDefault(pn => pn.MaPN == maPn);
                phieuNhap.MaNV = maVn;
                phieuNhap.MaTB = maTb;
                phieuNhap.NgayNhap = ngayNhap;
                phieuNhap.SoLuong = Int32.Parse(sl);
                phieuNhap.TinhTrang = tt;

                CapNhatSoLuongThietBi(maTb, _firstOrDefault.SoLuong, Int32.Parse(sl));
                _dataContext.SubmitChanges();

                LayDuLieuCacPhieuNhap();
                LayDuLieuThietBi();
            }
        }

        private void DienDuLieu(tblPhieuNhap phieuNhap)
        {
            txtmapn.Text = phieuNhap.MaPN;
            cbbmanv.SelectedIndex = cbbmanv.Items.IndexOf(phieuNhap.MaNV);
            cbbmatb.SelectedIndex = cbbmatb.Items.IndexOf(phieuNhap.MaTB);
            if (phieuNhap.NgayNhap != null) dtpngaynhap.Value = phieuNhap.NgayNhap.Value;
            txtsoluong.Text = phieuNhap.SoLuong.ToString();
            cbbtinhtrang.SelectedIndex = cbbtinhtrang.Items.IndexOf(phieuNhap.TinhTrang);
        }


        private void btnxoa_Click(object sender, EventArgs e)
        {
            if (dgwchitietphieunhap.SelectedRows.Count == 0) return;

            string maPn = dgwchitietphieunhap.SelectedRows[0].Cells[0].Value.ToString();

            if (YesNo(String.Format("BẠn có muốn xóa phiếu nhập có mã \"{0}\" không?", maPn)))
            {

                tblPhieuNhap firstOrDefault = _dataContext.tblPhieuNhaps.FirstOrDefault(pn => maPn == pn.MaPN);
                if (firstOrDefault != null)
                {
                    _dataContext.tblPhieuNhaps.DeleteOnSubmit(firstOrDefault);
                    _dataContext.SubmitChanges();

                    LayDuLieuCacPhieuNhap();
                }
            }
        }

        private tblPhieuNhap _firstOrDefault = null;
        private void dgwchitietphieunhap_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if(e.StateChanged != DataGridViewElementStates.Selected) return;
            if (dgwchitietphieunhap.SelectedRows.Count == 0) return;
            string maPN = dgwchitietphieunhap.SelectedRows[0].Cells[0].Value.ToString();

            _firstOrDefault = _dataContext.tblPhieuNhaps.FirstOrDefault(pn => maPN == pn.MaPN);

            DienDuLieu(_firstOrDefault);

        }
    }
}
