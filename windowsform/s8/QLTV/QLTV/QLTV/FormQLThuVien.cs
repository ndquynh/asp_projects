﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QLTV.Model;

namespace QLTV
{
    public partial class FormQLTV : Form
    {
        //Connection _connection = new Connection();
        QuanLyThuVien ctx = new QuanLyThuVien();

        /**
         * Being Edit: True
         * Else: False
         */
        Boolean PhieuMuonSavingFlag = false;

        Boolean ChiTietPhieuMuonSavingFlag = false;

        public FormQLTV()
        {
            InitializeComponent();
        }

        private void btnPMThem_Click(object sender, EventArgs e)
        {
            btnPMThem.Enabled = false;

            // PM
            txtMaPhieuMuon.Enabled = true;
            cbxPMMaDocGia.Enabled = true;
            dateTimePicker1.Enabled = true;
            btnPMLuu.Enabled = true;
            //btnPMXoa.Enabled = true;

            txtMaPhieuMuon.Focus();
        }

        private void btnPMLuu_Click(object sender, EventArgs e)
        {
            String maPhieuMuon = txtMaPhieuMuon.Text;
            var maDocGia = cbxPMMaDocGia.SelectedValue;
            var dateTime = dateTimePicker1.Value;

            if (PhieuMuonSavingFlag) // if being edited
            {
                var result = CheckPhieuMuonPrimaryKey(maPhieuMuon);
                if(result == null)
                    return;

                result.DocGia.MaDocGia = maDocGia.ToString();
                result.NgayMuon = dateTime;
                
                ctx.SubmitChanges();
            
                LoadDataPhieuMuon();
                // --------------------------
                btnPMThem.Enabled = true;
                // PM
                txtMaPhieuMuon.Enabled = false;
                cbxPMMaDocGia.Enabled = false;
                dateTimePicker1.Enabled = false;
                btnPMLuu.Enabled = false;
                MessageBox.Show("đã sửa dữ liệu", "Thành Công! ");
                LoadCtpmMaPhieuMuon();
            }
            else
            {
                if (CheckPhieuMuonPrimaryKey(maPhieuMuon) == null)
                {
                    DocGia docGia = GetDocGiaById(maDocGia.ToString());

                    if (docGia == null)
                    {
                        MessageBox.Show("Doc gia khong ton tai");
                        return;
                    }

                    ctx.PhieuMuons.InsertOnSubmit(new PhieuMuon {MaPhieuMuon = maPhieuMuon, DocGia = docGia, NgayMuon = dateTime });
                    
                    ctx.SubmitChanges();
                    LoadDataPhieuMuon();

                    // --------------------------
                    btnPMThem.Enabled = true;
                    // PM
                    txtMaPhieuMuon.Enabled = false;
                    cbxPMMaDocGia.Enabled = false;
                    dateTimePicker1.Enabled = false;
                    btnPMLuu.Enabled = false;
                    MessageBox.Show("đã thêm phiếu mượn mới", "Thành Công! ");
                    LoadCtpmMaPhieuMuon();
                }
                else
                {
                    MessageBox.Show("Mã Phiếu Mượn bị trùng lặp ", "Sai rồi! ");
                }

            }

            PhieuMuonSavingFlag = false;
        }

        private DocGia GetDocGiaById(string maDocGia)
        {
            return (from docgia in ctx.DocGias where docgia.MaDocGia == maDocGia select docgia).FirstOrDefault();
        }

        private PhieuMuon CheckPhieuMuonPrimaryKey(string primaryValue)
        {
            return (from phieuMuon in ctx.PhieuMuons where phieuMuon.MaPhieuMuon.Equals(primaryValue) select phieuMuon).FirstOrDefault();
        }

        private void btnPMXoa_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn thật sự muốn xóa?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (result == DialogResult.Yes)
            {
                int selectedrowindex = dataGVPM.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGVPM.Rows[selectedrowindex];
                if (selectedRow.Cells[0].Value == null)
                    return;

                string maPM = selectedRow.Cells[0].Value.ToString();
                var PM = (from phieuMuon in ctx.PhieuMuons where phieuMuon.MaPhieuMuon.Equals(maPM) select phieuMuon).FirstOrDefault();

                if (IsPrimaryKeyBeingForeignKey(PM))
                {
                    ctx.PhieuMuons.DeleteOnSubmit(PM);
                    ctx.SubmitChanges();

                    LoadDataPhieuMuon();
                }
                else
                {
                    MessageBox.Show("Tồn tại Chi tiết của phiếu mượn này", "Không thể xóa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else if (result == DialogResult.No)
            {
                
            }
        }

        private bool IsPrimaryKeyBeingForeignKey(PhieuMuon pm)
        {
            var result = (from chiTietPhieuMuon in ctx.ChiTietPMs where chiTietPhieuMuon.PhieuMuon.MaPhieuMuon.Equals(pm.MaPhieuMuon) select chiTietPhieuMuon).FirstOrDefault();

            return result == null; 
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
           // dateTimePicker1.slec
        }

        private void FormQLTV_Load(object sender, EventArgs e)
        {
            // PM
            EnablePhieuMuonInputs(false);
            btnPMLuu.Enabled = false;
            btnPMXoa.Enabled = false;

            // CTPM
            EnableChiTietPhieuMuonInputs(false);

            btnCTPMLuu.Enabled = false;
            btnCTPMXoa.Enabled = false;

            // Sum
            txtTongSoLuongMuon.Enabled = false;
            txtTongSoLuongMuon.Text = "0";
            txtTongSoLuongMuon.TextAlign = HorizontalAlignment.Center;
            btnThoat.Enabled = true;

            this.ControlBox = false;

            LoadDataPhieuMuon();
            LoadDataChiTietPhieuMuon();

            //cbx
            LoadPmDocGia();
            LoadCtpmMaPhieuMuon();
            LoadCtpmMaSach();
        }

       

        private void EnablePhieuMuonInputs(bool enable)
        {
            txtMaPhieuMuon.Enabled = enable;
            cbxPMMaDocGia.Enabled = enable;
            dateTimePicker1.Enabled = enable;
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void LoadDataPhieuMuon()
        {
            IEnumerable<Object> pms = (from pm in ctx.PhieuMuons select new { pm .MaPhieuMuon, pm.DocGia.MaDocGia, pm.NgayMuon}).ToList();
            dataGVPM.DataSource = pms;
        }

        private void dataGVPM_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (!dataGVPM.Focused)
            {
                btnPMXoa.Enabled = false;
                btnPMLuu.Enabled = false;
                btnPMThem.Enabled = true;
                EnablePhieuMuonInputs(false);
                return;
            }

            if (e.Cell.State == DataGridViewElementStates.Selected)
            {
                btnPMXoa.Enabled = true;
                btnPMLuu.Enabled = true;
                btnPMThem.Enabled = false;
                LoadDataPnToEdit();
                EnablePhieuMuonInputs(true);

            }
            else
            {
                btnPMXoa.Enabled = false;
                btnPMLuu.Enabled = false;
                btnPMThem.Enabled = true;
                EnablePhieuMuonInputs(false);
            }
        }
        private void dataGVPM_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (!dataGVPM.Focused)
            {
                btnPMXoa.Enabled = false;
                return;
            }
           
            //LoadDataPmToEdit();
            EnablePhieuMuonInputs(false);

        }

        private void LoadDataPmToEdit()
        {
            int selectedrowindex = dataGVPM.SelectedRows[0].Index;
            DataGridViewRow selectedRow = dataGVPM.Rows[selectedrowindex];
            string maPM = selectedRow.Cells[0].Value.ToString();
            var PM = (from phieuMuon in ctx.PhieuMuons where phieuMuon.MaPhieuMuon.Equals(maPM) select phieuMuon).FirstOrDefault();

            txtMaPhieuMuon.Text = PM.MaPhieuMuon;
            cbxPMMaDocGia.SelectedIndex = cbxPMMaDocGia.Items.IndexOf(PM.DocGia.MaDocGia);
            dateTimePicker1.Value = PM.NgayMuon;
        }

        private void LoadDataPnToEdit()
        {
            int selectedrowindex = dataGVPM.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGVPM.Rows[selectedrowindex];
            if (selectedRow.Cells[0].Value == null)
                return;
            string maPM = selectedRow.Cells[0].Value.ToString();
            var PM = (from phieuMuon in ctx.PhieuMuons where phieuMuon.MaPhieuMuon.Equals(maPM) select phieuMuon).FirstOrDefault() ;

            txtMaPhieuMuon.Text = PM?.MaPhieuMuon;
            var dg = PM?.DocGia;
            var index = cbxPMMaDocGia.Items.IndexOf(dg.MaDocGia);
            cbxPMMaDocGia.SelectedIndex = index;
            dateTimePicker1.Value = PM.NgayMuon;

            PhieuMuonSavingFlag = true;
        }
       
        private void cbxPMMaDocGia_Enter(object sender, EventArgs e)
        {
            //LoadPmDocGia();
        }

        private void LoadPmDocGia()
        {
            IEnumerable<String> maDocGias = from docGia in ctx.DocGias select docGia.MaDocGia;
            cbxPMMaDocGia.DataSource = maDocGias;
        }


        /*--------------------------------------------------------------------------------------------------------
         *                               ChiTietPhieuMuon
         * ------------------------------------------------------------------------------------------------------
         */

        /**
         * */
        private void EnableChiTietPhieuMuonInputs(bool enable)
        {
            cbxCTPMMaPhieuMuon.Enabled = enable;
            cbxCTPMMaSach.Enabled = enable;
            txtCTPMSoLuong.Enabled = enable;
        }

        void LoadDataChiTietPhieuMuon()
        {
            IEnumerable<Object> ctpms = (from ctpm in ctx.ChiTietPMs select new {ctpm.PhieuMuon.MaPhieuMuon, ctpm.Sach.MaSach, ctpm.SoLuong}).ToList();
            dataGVCTPM.DataSource = ctpms;
        }

        private void btnCTPMThem_Click(object sender, EventArgs e)
        {
            btnCTPMThem.Enabled = false;

            // CTPM
            EnableChiTietPhieuMuonInputs(true);
            btnCTPMLuu.Enabled = true;
            btnCTPMXoa.Enabled = false;
        }

        private int? ConvertStringToInt(string intString)
        {
            int i = 0;
            return (Int32.TryParse(intString, out i) ? i : (int?)null);
        }

        private void btnCTPMLuu_Click(object sender, EventArgs e)
        {
            String maPhieuMuon = cbxCTPMMaPhieuMuon.SelectedValue.ToString();
            var maSach = cbxCTPMMaSach.SelectedValue.ToString();
            var soLuong = txtCTPMSoLuong.Text;
            if (soLuong.Length <= 0)
            {
                MessageBox.Show("Số lượng không được phép bỏ trống", "lỗi");
                return;
            }

            if (ChiTietPhieuMuonSavingFlag) // if being edited
            {
                if (chiTietPhieuMuon == null)
                {
                    return;
                }

                var result = CheckChiTietPhieuMuonPrimaryKey(chiTietPhieuMuon.PhieuMuon.MaPhieuMuon, chiTietPhieuMuon.Sach.MaSach);
                if (result == null)
                    return;

                result.PhieuMuon = ctx.PhieuMuons.Single(c => c.MaPhieuMuon == maPhieuMuon); ;
                result.Sach = ctx.Sachs.Single(c => c.MaSach == maSach);
                result.SoLuong = (int)ConvertStringToInt(soLuong);

                ctx.SubmitChanges();

                LoadDataChiTietPhieuMuon();
                // --------------------------
                btnCTPMThem.Enabled = true;
                // PM
                EnableChiTietPhieuMuonInputs(false);

                btnCTPMLuu.Enabled = false;
                MessageBox.Show("đã sửa dữ liệu", "Thành Công! ");
                LoadCtpmMaPhieuMuon();
            }
            else
            {
                if (CheckChiTietPhieuMuonPrimaryKey(maPhieuMuon, maSach) == null)
                {
                    ctx.ChiTietPMs.InsertOnSubmit(new ChiTietPhieuMuon { PhieuMuon = (from pm in ctx.PhieuMuons where  pm.MaPhieuMuon == maPhieuMuon select pm)?.FirstOrDefault(), Sach = (from sach in ctx.Sachs where sach.MaSach == maPhieuMuon select sach)?.FirstOrDefault(), SoLuong =  (int)ConvertStringToInt(soLuong) });

                    ctx.SubmitChanges();
                    LoadDataChiTietPhieuMuon();

                    // --------------------------
                    btnCTPMThem.Enabled = true;
                    // PM
                    EnableChiTietPhieuMuonInputs(false);
                    btnCTPMLuu.Enabled = false;
                    MessageBox.Show("đã thêm chi tiet phiếu mượn mới", "Thành Công! ");
                    //LoadCtpmMaPhieuMuon();
                }
                else
                {
                    MessageBox.Show("Mã chi tiet Phiếu Mượn bị trùng lặp ", "Sai rồi! ");
                }

            }

            ChiTietPhieuMuonSavingFlag = false;
        }

        private void btnCTPMXoa_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn thật sự muốn xóa?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (result == DialogResult.Yes)
            {
                int selectedrowindex = dataGVCTPM.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGVCTPM.Rows[selectedrowindex];
                if (selectedRow.Cells[0].Value == null)
                    return;
                string maPM = selectedRow.Cells[0].Value.ToString();
                var ct = (from ctpm in ctx.ChiTietPMs where ctpm.PhieuMuon.MaPhieuMuon.Equals(maPM) select ctpm).FirstOrDefault();
 
                    ctx.ChiTietPMs.DeleteOnSubmit(ct);
                    ctx.SubmitChanges();

                    LoadDataChiTietPhieuMuon();
                    MessageBox.Show("Xoa Thanh cong", ct.PhieuMuon.MaPhieuMuon, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (result == DialogResult.No)
            {

            }
        }
        
       private void cbxCTPMMaPhieuMuon_Enter(object sender, EventArgs e)
        {
           // LoadCtpmMaPhieuMuon();
        }

        public void LoadCtpmMaPhieuMuon()
        {
            IEnumerable<String> maPhieuMuons = from phieuMuon in ctx.PhieuMuons select phieuMuon.MaPhieuMuon;
            cbxCTPMMaPhieuMuon.DataSource = maPhieuMuons;
        }

        private void cbxCTPMMaSach_Enter(object sender, EventArgs e)
        {
            //LoadCtpmMaSach();
        }

        private void LoadCtpmMaSach()
        {
            IEnumerable<String> maSachs = from sach in ctx.Sachs select sach.MaSach;
            cbxCTPMMaSach.DataSource = maSachs;
        }

        private void dataGVCTPM_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (!dataGVCTPM.Focused)
            {
                btnCTPMXoa.Enabled = false;
                btnCTPMLuu.Enabled = false;
                btnCTPMThem.Enabled = true;
                EnableChiTietPhieuMuonInputs(false);
                chiTietPhieuMuon = null;
                return;
            }

            if (e.Cell.State == DataGridViewElementStates.Selected)
            {
                btnCTPMXoa.Enabled = true;
                btnCTPMLuu.Enabled = true;
                btnCTPMThem.Enabled = false;
                LoadDataCtpmToEdit();
                EnableChiTietPhieuMuonInputs(true);

            }
            else
            {
                btnCTPMXoa.Enabled = false;
                btnCTPMLuu.Enabled = false;
                btnCTPMThem.Enabled = true;
                EnableChiTietPhieuMuonInputs(false);
                chiTietPhieuMuon = null;
            }
        }


        private ChiTietPhieuMuon chiTietPhieuMuon;
        private void LoadDataCtpmToEdit()
        {
            int selectedrowindex = dataGVCTPM.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGVCTPM.Rows[selectedrowindex];
            if (selectedRow.Cells[0].Value == null)
                return;
            string maPM = selectedRow.Cells[0].Value.ToString();
            string maSach = selectedRow.Cells[1].Value.ToString();
            var ct = CheckChiTietPhieuMuonPrimaryKey(maPM, maSach);
            chiTietPhieuMuon = ct;

            cbxCTPMMaPhieuMuon.SelectedIndex = cbxCTPMMaPhieuMuon.Items.IndexOf(ct.PhieuMuon.MaPhieuMuon);
            cbxCTPMMaPhieuMuon.Update();
            cbxCTPMMaSach.SelectedIndex = cbxCTPMMaSach.Items.IndexOf(ct.Sach.MaSach);
            cbxCTPMMaSach.Update();
            txtCTPMSoLuong.Text = ct.SoLuong.ToString();

            ChiTietPhieuMuonSavingFlag = true;
        }

        private ChiTietPhieuMuon CheckChiTietPhieuMuonPrimaryKey(string primaryValue1,string primaryValue2)
        {
            return (from ctpm in ctx.ChiTietPMs where (ctpm.PhieuMuon.MaPhieuMuon.Equals(primaryValue1) && ctpm.Sach.MaSach.Equals(primaryValue2)) select ctpm).FirstOrDefault();
        }

        private void dataGVCTPM_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (!dataGVCTPM.Focused)
            {
                btnCTPMXoa.Enabled = false;
                return;
            }

            //LoadDataPmToEdit();
            EnableChiTietPhieuMuonInputs(false);
        }

       
    }
}
