﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLTV
{
    public class Connection
    {
        private SqlConnection mConnection;

        public Connection()
        {

        }

        public String GetConnectionString()
        {
            String username = ConfigurationManager.AppSettings["dbUsername"];
            String password = ConfigurationManager.AppSettings["dbPassword"];
            String dataSource = ConfigurationManager.AppSettings["dbSource"];


            return BuildConnectionString(dataSource, username, password).ToString();
        }

        public void OpenConnection()
        {

            if (mConnection == null)
            {
                mConnection = new SqlConnection(GetConnectionString().ToString());
            }

            if (mConnection.State != ConnectionState.Open)
            {
                try
                {
                    mConnection.Open();
                    Console.WriteLine("Connection Open");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.WriteLine("Cannot Open Connection: " + mConnection.ConnectionString);
                }
            }
        }

        private SqlConnectionStringBuilder BuildConnectionString(string dataSource, string userName, string userPassword)
        {


            // Retrieve the partial connection string named databaseConnection
            // from the application's app.config or web.config file.
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["remote_aws_address"];
            
            if (null != settings)
            {
                // Retrieve the partial connection string.
                string connectString = settings.ConnectionString;
                Console.WriteLine("Original: {0}", connectString);

                // Create a new SqlConnectionStringBuilder based on the
                // partial connection string retrieved from the config file.
                SqlConnectionStringBuilder builder =
                    new SqlConnectionStringBuilder(connectString);

                // Supply the additional values.
                builder.DataSource = dataSource;
                builder.UserID = userName;
                builder.Password = userPassword;
                try
                {
                    builder.NetworkLibrary = ConfigurationManager.AppSettings["Network Library"];

                }
                catch (ConfigurationErrorsException errorsException)
                {
                    Console.WriteLine(errorsException);
//                    throw;
                }
                
                Console.WriteLine("Modified: {0}", builder.ConnectionString);

                return builder;
            }

            return null;
        }
    }
}
