﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace QLTV.Model
{
    [Table(Name = "ChiTietPhieuMuon")]
    public class ChiTietPhieuMuon
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = false, Name = "MaPhieuMuon")]
        private String _maPhieuMuon;
        private EntityRef<PhieuMuon> _phieuMuonRef = new EntityRef<PhieuMuon>();
        [Association(Name = "FK_ChiTietPhieuMuon_PhieuMuon", IsForeignKey = true, Storage = "_phieuMuonRef", ThisKey = "_maPhieuMuon")]
        public PhieuMuon PhieuMuon
        {
            get {return _phieuMuonRef.Entity; }
            set
            {
                PhieuMuon priorPhieuMuon = _phieuMuonRef.Entity;
                PhieuMuon newPhieuMuon = value;

                if (priorPhieuMuon != newPhieuMuon)
                {
                    _phieuMuonRef.Entity = null;
                    priorPhieuMuon?.



                }
            }
        }


        [Column(IsPrimaryKey = true, Name = "MaSach")]
        private String _maSach;
        private EntityRef<Sach> _sachRef = new EntityRef<Sach>();
        [Association(Name = "FK_ChiTietPhieuMuon_Sach", IsForeignKey = true, Storage = "_sachRef", ThisKey = "_maSach")]
        public Sach Sach
        {
            get => _sachRef.Entity;
            set => _sachRef.Entity = value;
        }
        [Column]
        public int SoLuong { get; set; }
    }
}