﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace QLTV.Model
{
    [Database(Name = "QuanLyThuVien")]
    public class QuanLyThuVien : DataContext
    {
        private static String connStr = new QLTV.Connection().GetConnectionString();

        public QuanLyThuVien() : base(connStr)
        {
            
        }
        
        public Table<Sach> Sachs;
        public Table<DocGia> DocGias;
        public Table<Nxb> Nxbs;
        public Table<PhieuMuon> PhieuMuons;
        public Table<ChiTietPhieuMuon> ChiTietPMs;

    }
}//https://www.codeproject.com/Articles/43025/A-LINQ-Tutorial-Mapping-Tables-to-Objects
