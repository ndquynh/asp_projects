﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLTV.Model
{
    [Table(Name = "Sach")]
    public class Sach
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = false, Name = "MaSach")] 
        public String MaSach { get; set; }

        [Column(Name = "TenSach")]
        public String TenSach { get; set; }

        [Column]
        public String TacGia { get; set; }


        [Column(Name = "MaNXB")] private String _maNXB;
        private EntityRef<Nxb> _nxb = new EntityRef<Nxb>();
        [Association(Name = "FK_Sachs_Nxbs"    , IsForeignKey = true, Storage = "_nxb", ThisKey = "_maNXB")]
        public Nxb Nxb
        {
            get { return _nxb.Entity; }
            set { _nxb.Entity = value; }
        }

        [Column]
        public int SoLuong { get; set; }
    }
}
