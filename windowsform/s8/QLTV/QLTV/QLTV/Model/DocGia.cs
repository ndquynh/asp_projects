﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace QLTV.Model
{
    [Table(Name = "DocGia")]
    public class DocGia
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = false, Name = "MaDocGia")]
        public String MaDocGia { get; set; }

        [Column]
        public String TenDocGia { get; set; }

        [Column]
        public String DiaChi { get; set; }
        [Column]
        public String DienThoai { get; set; }

        [Column]
        public DateTime NgayLapThe { get; set; }

        [Column]
        public DateTime NgayHetHan { get; set; }

        private EntitySet<PhieuMuon> _phieuMuons = new EntitySet<PhieuMuon>();
        [Association(Name = "FK_PhieuMuons_DocGias", Storage = "_phieuMuons", OtherKey = "_maDocGia", ThisKey = "MaDocGia")]
        public ICollection<PhieuMuon> PhieuMuons {
            get => _phieuMuons;
            set => _phieuMuons.Assign(value);
        }

    }
}