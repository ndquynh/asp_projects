﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace QLTV.Model
{
    [Table(Name = "NXB")]
    public class Nxb
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = false, Name = "MaNXB")]
        public String MaNXB { get; set; }

        [Column]
        public String TenNXB { get; set; }

        [Column]
        public String DiaChi { get; set; }

        /***
         * Backing field that references the Sachs table
         */
        private EntitySet<Sach> _saches = new EntitySet<Sach>();

        [Association(Name = "FK_Sachs_Nxbs", Storage = "_saches", OtherKey = "_maNXB", ThisKey = "MaNXB")]
        public ICollection<Sach> Saches
        {
            get { return _saches; }
            set
            {
                _saches.Assign(value);
            }
        }

    }
}