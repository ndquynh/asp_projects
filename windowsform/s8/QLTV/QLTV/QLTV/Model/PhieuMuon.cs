﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace QLTV.Model
{
    [Table(Name = "PhieuMuon")]
    public class PhieuMuon
    {

        [Column(IsPrimaryKey = true, IsDbGenerated = false, Name = "MaPhieuMuon")]
        public String MaPhieuMuon { get; set; }

        private EntitySet<ChiTietPhieuMuon> _chiTietPhieuMuons = new EntitySet<ChiTietPhieuMuon>();


        [Column(Name = "MaDocGia")] private String _maDocGia;
        private EntityRef<DocGia> _docGia = new EntityRef<DocGia>();
        [Association(Name = "FK_PhieuMuons_DocGias", IsForeignKey = true, Storage = "_docGia", ThisKey = "_maDocGia")]
        public DocGia DocGia
        {
            get => _docGia.Entity;
//            set => _docGia.Entity = value;
            set
            {
                DocGia priorDocGia = _docGia.Entity;
                DocGia newDocGia = value;

                if (priorDocGia != newDocGia)
                {
                    _docGia.Entity = null;
                    priorDocGia?.PhieuMuons.Remove(this);

                    _docGia.Entity = newDocGia;

                    newDocGia?.PhieuMuons.Add(this);
                }
            }
        }

        [Column]
        public DateTime NgayMuon { get; set; }

    }
}