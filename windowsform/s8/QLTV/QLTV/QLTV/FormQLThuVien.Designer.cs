﻿namespace QLTV
{
    partial class FormQLTV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPMXoa = new System.Windows.Forms.Button();
            this.btnPMLuu = new System.Windows.Forms.Button();
            this.btnPMThem = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dataGVPM = new System.Windows.Forms.DataGridView();
            this.cbxPMMaDocGia = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaPhieuMuon = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbxCTPMMaPhieuMuon = new System.Windows.Forms.ComboBox();
            this.txtCTPMSoLuong = new System.Windows.Forms.TextBox();
            this.btnCTPMXoa = new System.Windows.Forms.Button();
            this.btnCTPMLuu = new System.Windows.Forms.Button();
            this.btnCTPMThem = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGVCTPM = new System.Windows.Forms.DataGridView();
            this.cbxCTPMMaSach = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnThoat = new System.Windows.Forms.Button();
            this.txtTongSoLuongMuon = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVPM)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVCTPM)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPMXoa);
            this.groupBox1.Controls.Add(this.btnPMLuu);
            this.groupBox1.Controls.Add(this.btnPMThem);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.dataGVPM);
            this.groupBox1.Controls.Add(this.cbxPMMaDocGia);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtMaPhieuMuon);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(754, 225);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Phiếu Mượn";
            // 
            // btnPMXoa
            // 
            this.btnPMXoa.Image = global::QLTV.Properties.Resources.tsbDelete;
            this.btnPMXoa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPMXoa.Location = new System.Drawing.Point(241, 151);
            this.btnPMXoa.Name = "btnPMXoa";
            this.btnPMXoa.Size = new System.Drawing.Size(93, 42);
            this.btnPMXoa.TabIndex = 10;
            this.btnPMXoa.Text = "Xóa";
            this.btnPMXoa.UseVisualStyleBackColor = true;
            this.btnPMXoa.Click += new System.EventHandler(this.btnPMXoa_Click);
            // 
            // btnPMLuu
            // 
            this.btnPMLuu.Image = global::QLTV.Properties.Resources.tsbSave;
            this.btnPMLuu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPMLuu.Location = new System.Drawing.Point(128, 151);
            this.btnPMLuu.Name = "btnPMLuu";
            this.btnPMLuu.Size = new System.Drawing.Size(93, 42);
            this.btnPMLuu.TabIndex = 9;
            this.btnPMLuu.Text = "Lưu";
            this.btnPMLuu.UseVisualStyleBackColor = true;
            this.btnPMLuu.Click += new System.EventHandler(this.btnPMLuu_Click);
            // 
            // btnPMThem
            // 
            this.btnPMThem.Image = global::QLTV.Properties.Resources.tsbAddNew;
            this.btnPMThem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPMThem.Location = new System.Drawing.Point(12, 151);
            this.btnPMThem.Name = "btnPMThem";
            this.btnPMThem.Size = new System.Drawing.Size(93, 42);
            this.btnPMThem.TabIndex = 8;
            this.btnPMThem.Text = "Thêm";
            this.btnPMThem.UseVisualStyleBackColor = true;
            this.btnPMThem.Click += new System.EventHandler(this.btnPMThem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mã phiếu mượn";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(128, 89);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(206, 20);
            this.dateTimePicker1.TabIndex = 6;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dataGVPM
            // 
            this.dataGVPM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGVPM.Location = new System.Drawing.Point(353, 19);
            this.dataGVPM.Name = "dataGVPM";
            this.dataGVPM.Size = new System.Drawing.Size(395, 200);
            this.dataGVPM.TabIndex = 5;
            this.dataGVPM.CellStateChanged += new System.Windows.Forms.DataGridViewCellStateChangedEventHandler(this.dataGVPM_CellStateChanged);
            this.dataGVPM.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dataGVPM_RowStateChanged);
            // 
            // cbxPMMaDocGia
            // 
            this.cbxPMMaDocGia.FormattingEnabled = true;
            this.cbxPMMaDocGia.Location = new System.Drawing.Point(128, 62);
            this.cbxPMMaDocGia.Name = "cbxPMMaDocGia";
            this.cbxPMMaDocGia.Size = new System.Drawing.Size(206, 21);
            this.cbxPMMaDocGia.TabIndex = 4;
            this.cbxPMMaDocGia.Enter += new System.EventHandler(this.cbxPMMaDocGia_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mã Độc Giả";
            // 
            // txtMaPhieuMuon
            // 
            this.txtMaPhieuMuon.Location = new System.Drawing.Point(128, 36);
            this.txtMaPhieuMuon.Name = "txtMaPhieuMuon";
            this.txtMaPhieuMuon.Size = new System.Drawing.Size(206, 20);
            this.txtMaPhieuMuon.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã phiếu mượn";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbxCTPMMaPhieuMuon);
            this.groupBox2.Controls.Add(this.txtCTPMSoLuong);
            this.groupBox2.Controls.Add(this.btnCTPMXoa);
            this.groupBox2.Controls.Add(this.btnCTPMLuu);
            this.groupBox2.Controls.Add(this.btnCTPMThem);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.dataGVCTPM);
            this.groupBox2.Controls.Add(this.cbxCTPMMaSach);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(15, 244);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(754, 225);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chi Tiết Phiếu Mượn";
            // 
            // cbxCTPMMaPhieuMuon
            // 
            this.cbxCTPMMaPhieuMuon.FormattingEnabled = true;
            this.cbxCTPMMaPhieuMuon.Location = new System.Drawing.Point(128, 36);
            this.cbxCTPMMaPhieuMuon.Name = "cbxCTPMMaPhieuMuon";
            this.cbxCTPMMaPhieuMuon.Size = new System.Drawing.Size(206, 21);
            this.cbxCTPMMaPhieuMuon.TabIndex = 12;
            this.cbxCTPMMaPhieuMuon.Enter += new System.EventHandler(this.cbxCTPMMaPhieuMuon_Enter);
            // 
            // txtCTPMSoLuong
            // 
            this.txtCTPMSoLuong.Location = new System.Drawing.Point(128, 92);
            this.txtCTPMSoLuong.Name = "txtCTPMSoLuong";
            this.txtCTPMSoLuong.Size = new System.Drawing.Size(206, 20);
            this.txtCTPMSoLuong.TabIndex = 11;
            // 
            // btnCTPMXoa
            // 
            this.btnCTPMXoa.Image = global::QLTV.Properties.Resources.tsbDelete;
            this.btnCTPMXoa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCTPMXoa.Location = new System.Drawing.Point(241, 151);
            this.btnCTPMXoa.Name = "btnCTPMXoa";
            this.btnCTPMXoa.Size = new System.Drawing.Size(93, 42);
            this.btnCTPMXoa.TabIndex = 10;
            this.btnCTPMXoa.Text = "Xóa";
            this.btnCTPMXoa.UseVisualStyleBackColor = true;
            this.btnCTPMXoa.Click += new System.EventHandler(this.btnCTPMXoa_Click);
            // 
            // btnCTPMLuu
            // 
            this.btnCTPMLuu.Image = global::QLTV.Properties.Resources.tsbSave;
            this.btnCTPMLuu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCTPMLuu.Location = new System.Drawing.Point(128, 151);
            this.btnCTPMLuu.Name = "btnCTPMLuu";
            this.btnCTPMLuu.Size = new System.Drawing.Size(93, 42);
            this.btnCTPMLuu.TabIndex = 9;
            this.btnCTPMLuu.Text = "Lưu";
            this.btnCTPMLuu.UseVisualStyleBackColor = true;
            this.btnCTPMLuu.Click += new System.EventHandler(this.btnCTPMLuu_Click);
            // 
            // btnCTPMThem
            // 
            this.btnCTPMThem.Image = global::QLTV.Properties.Resources.tsbAddNew;
            this.btnCTPMThem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCTPMThem.Location = new System.Drawing.Point(12, 151);
            this.btnCTPMThem.Name = "btnCTPMThem";
            this.btnCTPMThem.Size = new System.Drawing.Size(93, 42);
            this.btnCTPMThem.TabIndex = 8;
            this.btnCTPMThem.Text = "Thêm";
            this.btnCTPMThem.UseVisualStyleBackColor = true;
            this.btnCTPMThem.Click += new System.EventHandler(this.btnCTPMThem_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Số Lượng";
            // 
            // dataGVCTPM
            // 
            this.dataGVCTPM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGVCTPM.Location = new System.Drawing.Point(353, 19);
            this.dataGVCTPM.Name = "dataGVCTPM";
            this.dataGVCTPM.Size = new System.Drawing.Size(395, 200);
            this.dataGVCTPM.TabIndex = 5;
            this.dataGVCTPM.CellStateChanged += new System.Windows.Forms.DataGridViewCellStateChangedEventHandler(this.dataGVCTPM_CellStateChanged);
            this.dataGVCTPM.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dataGVCTPM_RowStateChanged);
            // 
            // cbxCTPMMaSach
            // 
            this.cbxCTPMMaSach.FormattingEnabled = true;
            this.cbxCTPMMaSach.Location = new System.Drawing.Point(128, 62);
            this.cbxCTPMMaSach.Name = "cbxCTPMMaSach";
            this.cbxCTPMMaSach.Size = new System.Drawing.Size(206, 21);
            this.cbxCTPMMaSach.TabIndex = 4;
            this.cbxCTPMMaSach.Enter += new System.EventHandler(this.cbxCTPMMaSach_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Mã sách";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Mã phiếu mượn";
            // 
            // btnThoat
            // 
            this.btnThoat.Image = global::QLTV.Properties.Resources.tsbClose;
            this.btnThoat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThoat.Location = new System.Drawing.Point(676, 510);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(93, 42);
            this.btnThoat.TabIndex = 11;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // txtTongSoLuongMuon
            // 
            this.txtTongSoLuongMuon.Location = new System.Drawing.Point(476, 475);
            this.txtTongSoLuongMuon.Name = "txtTongSoLuongMuon";
            this.txtTongSoLuongMuon.Size = new System.Drawing.Size(290, 20);
            this.txtTongSoLuongMuon.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(365, 482);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Tổng số lượng mượn";
            // 
            // FormQLTV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 564);
            this.Controls.Add(this.txtTongSoLuongMuon);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormQLTV";
            this.Text = "Quản Lý Thư Viện";
            this.Load += new System.EventHandler(this.FormQLTV_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVPM)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVCTPM)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DataGridView dataGVPM;
        private System.Windows.Forms.ComboBox cbxPMMaDocGia;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMaPhieuMuon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPMThem;
        private System.Windows.Forms.Button btnPMXoa;
        private System.Windows.Forms.Button btnPMLuu;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCTPMXoa;
        private System.Windows.Forms.Button btnCTPMLuu;
        private System.Windows.Forms.Button btnCTPMThem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGVCTPM;
        private System.Windows.Forms.ComboBox cbxCTPMMaSach;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbxCTPMMaPhieuMuon;
        private System.Windows.Forms.TextBox txtCTPMSoLuong;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.TextBox txtTongSoLuongMuon;
        private System.Windows.Forms.Label label7;
    }
}

