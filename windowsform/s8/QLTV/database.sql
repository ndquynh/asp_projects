USE [master]
GO
/****** Object:  Database [QuanLyThuVien]    Script Date: 27/09/2018 8:22:43 PM ******/
CREATE DATABASE [QuanLyThuVien]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QuanLyThuVien', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\QuanLyThuVien.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QuanLyThuVien_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\QuanLyThuVien_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QuanLyThuVien] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QuanLyThuVien].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QuanLyThuVien] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET ARITHABORT OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [QuanLyThuVien] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QuanLyThuVien] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QuanLyThuVien] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QuanLyThuVien] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QuanLyThuVien] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET RECOVERY FULL 
GO
ALTER DATABASE [QuanLyThuVien] SET  MULTI_USER 
GO
ALTER DATABASE [QuanLyThuVien] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QuanLyThuVien] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QuanLyThuVien] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QuanLyThuVien] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'QuanLyThuVien', N'ON'
GO
USE [QuanLyThuVien]
GO
/****** Object:  Table [dbo].[ChiTietPhieuMuon]    Script Date: 27/09/2018 8:22:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChiTietPhieuMuon](
	[MaPhieuMuon] [varchar](10) NOT NULL,
	[MaSach] [varchar](10) NOT NULL,
	[SoLuong] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaPhieuMuon] ASC,
	[MaSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocGia]    Script Date: 27/09/2018 8:22:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocGia](
	[MaDocGia] [varchar](10) NOT NULL,
	[TenDocGia] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](50) NULL,
	[DienThoai] [varchar](11) NULL,
	[NgayLapThe] [datetime] NULL,
	[NgayHetHan] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaDocGia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NXB]    Script Date: 27/09/2018 8:22:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NXB](
	[MaNXB] [varchar](10) NOT NULL,
	[TenNXB] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MaNXB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhieuMuon]    Script Date: 27/09/2018 8:22:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhieuMuon](
	[MaPhieuMuon] [varchar](10) NOT NULL,
	[MaDocGia] [varchar](10) NULL,
	[NgayMuon] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaPhieuMuon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sach]    Script Date: 27/09/2018 8:22:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sach](
	[MaSach] [varchar](10) NOT NULL,
	[TenSach] [nvarchar](50) NULL,
	[TacGia] [nvarchar](50) NULL,
	[MaNXB] [varchar](10) NULL,
	[SoLuong] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[ChiTietPhieuMuon] ([MaPhieuMuon], [MaSach], [SoLuong]) VALUES (N'mpm', N'masach', 1)
INSERT [dbo].[ChiTietPhieuMuon] ([MaPhieuMuon], [MaSach], [SoLuong]) VALUES (N'mpm1', N'masach1', 1)
INSERT [dbo].[DocGia] ([MaDocGia], [TenDocGia], [DiaChi], [DienThoai], [NgayLapThe], [NgayHetHan]) VALUES (N'1', N'dg1', N'diachi1', N'dt1', CAST(0x0000911500000000 AS DateTime), CAST(0x0000953E00000000 AS DateTime))
INSERT [dbo].[DocGia] ([MaDocGia], [TenDocGia], [DiaChi], [DienThoai], [NgayLapThe], [NgayHetHan]) VALUES (N'2', N'dg2', N'diachi2', N'dt2', CAST(0x0000911500000000 AS DateTime), CAST(0x0000953E00000000 AS DateTime))
INSERT [dbo].[DocGia] ([MaDocGia], [TenDocGia], [DiaChi], [DienThoai], [NgayLapThe], [NgayHetHan]) VALUES (N'docgia', N'tendocgia', N'diachi', N'dt', CAST(0x0000907600000000 AS DateTime), CAST(0x000094A000000000 AS DateTime))
INSERT [dbo].[DocGia] ([MaDocGia], [TenDocGia], [DiaChi], [DienThoai], [NgayLapThe], [NgayHetHan]) VALUES (N'docgia3', N'dg3', N'diachi3', N'dt3', CAST(0x0000907600000000 AS DateTime), CAST(0x000094A000000000 AS DateTime))
INSERT [dbo].[DocGia] ([MaDocGia], [TenDocGia], [DiaChi], [DienThoai], [NgayLapThe], [NgayHetHan]) VALUES (N'docgia4', N'tendocgia4', N'diachi4', N'dt3', CAST(0x0000907600000000 AS DateTime), CAST(0x000094A000000000 AS DateTime))
INSERT [dbo].[DocGia] ([MaDocGia], [TenDocGia], [DiaChi], [DienThoai], [NgayLapThe], [NgayHetHan]) VALUES (N'docgia5', N'tendocgia5', N'diachi5', N'dt5', CAST(0x0000907600000000 AS DateTime), CAST(0x000094A000000000 AS DateTime))
INSERT [dbo].[DocGia] ([MaDocGia], [TenDocGia], [DiaChi], [DienThoai], [NgayLapThe], [NgayHetHan]) VALUES (N'docgia6', N'tendocgia6', N'diachi6', N'dt6', CAST(0x0000907600000000 AS DateTime), CAST(0x000094A000000000 AS DateTime))
INSERT [dbo].[NXB] ([MaNXB], [TenNXB], [DiaChi]) VALUES (N'nxb', N'ten nxb', N'dia chi nxb')
INSERT [dbo].[NXB] ([MaNXB], [TenNXB], [DiaChi]) VALUES (N'nxb1', N'ten nxb1', N'dia chi nxb1')
INSERT [dbo].[NXB] ([MaNXB], [TenNXB], [DiaChi]) VALUES (N'nxb2', N'ten nxb2', N'dia chi nxb2')
INSERT [dbo].[NXB] ([MaNXB], [TenNXB], [DiaChi]) VALUES (N'nxb3', N'ten nxb3', N'dia chi nxb3')
INSERT [dbo].[PhieuMuon] ([MaPhieuMuon], [MaDocGia], [NgayMuon]) VALUES (N'mpm', N'2', CAST(0x00009E3400000000 AS DateTime))
INSERT [dbo].[PhieuMuon] ([MaPhieuMuon], [MaDocGia], [NgayMuon]) VALUES (N'mpm1', N'1', CAST(0x00009E3400000000 AS DateTime))
INSERT [dbo].[PhieuMuon] ([MaPhieuMuon], [MaDocGia], [NgayMuon]) VALUES (N'mpm2', N'docgia4', CAST(0x00009E3400000000 AS DateTime))
INSERT [dbo].[PhieuMuon] ([MaPhieuMuon], [MaDocGia], [NgayMuon]) VALUES (N'mpm3', N'2', CAST(0x00009E3400000000 AS DateTime))
INSERT [dbo].[PhieuMuon] ([MaPhieuMuon], [MaDocGia], [NgayMuon]) VALUES (N'mpm4', N'docgia5', CAST(0x00009E3400000000 AS DateTime))
INSERT [dbo].[PhieuMuon] ([MaPhieuMuon], [MaDocGia], [NgayMuon]) VALUES (N'mpm5', N'docgia6', CAST(0x00009E3400000000 AS DateTime))
INSERT [dbo].[Sach] ([MaSach], [TenSach], [TacGia], [MaNXB], [SoLuong]) VALUES (N'masach', N'tensach', N'tg', N'nxb', 1)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [TacGia], [MaNXB], [SoLuong]) VALUES (N'masach1', N'tensach1', N'tg1', N'nxb1', 1)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [TacGia], [MaNXB], [SoLuong]) VALUES (N'masach2', N'tensach2', N'tg2', N'nxb2', 11)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [TacGia], [MaNXB], [SoLuong]) VALUES (N'masach3', N'tensach3', N'tg3', N'nxb3', 5)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [TacGia], [MaNXB], [SoLuong]) VALUES (N'masach4', N'tensach4', N'tg2', N'nxb2', 7)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [TacGia], [MaNXB], [SoLuong]) VALUES (N'masach5', N'tensach5', N'tg2', N'nxb2', 1)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [TacGia], [MaNXB], [SoLuong]) VALUES (N'masach6', N'tensach6', N'tg1', N'nxb3', 11)
ALTER TABLE [dbo].[ChiTietPhieuMuon]  WITH CHECK ADD FOREIGN KEY([MaPhieuMuon])
REFERENCES [dbo].[PhieuMuon] ([MaPhieuMuon])
GO
ALTER TABLE [dbo].[ChiTietPhieuMuon]  WITH CHECK ADD FOREIGN KEY([MaSach])
REFERENCES [dbo].[Sach] ([MaSach])
GO
ALTER TABLE [dbo].[PhieuMuon]  WITH CHECK ADD FOREIGN KEY([MaDocGia])
REFERENCES [dbo].[DocGia] ([MaDocGia])
GO
ALTER TABLE [dbo].[Sach]  WITH CHECK ADD FOREIGN KEY([MaNXB])
REFERENCES [dbo].[NXB] ([MaNXB])
GO
USE [master]
GO
ALTER DATABASE [QuanLyThuVien] SET  READ_WRITE 
GO
