﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFormWithDB
{
    public partial class ServerConfigForm : Form
    {
        public ServerConfigForm()
        {
            InitializeComponent();

            //ThreadStart childref = new ThreadStart(loadServerName);
            //Console.WriteLine("In Main: Creating the Child thread");
            //Thread childThread = new Thread(childref);
            //childThread.Start();
            loadServerName();
        }

        void loadServerName()
        {
            cbxServerName.DataSource = CauHinh.GetServerName();
            cbxServerName.DisplayMember = "ServerName";
        }

        private void cbxServerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ThreadStart childref = new ThreadStart(loadDBName);
            //Console.WriteLine("In Main: Creating the Child thread");
            //Thread childThread = new Thread(childref);
            //childThread.Start();
            
        }

        void loadDBName()
        {
            cbxDatabase.DataSource = CauHinh.GetDBName(cbxServerName.Text, tbxUserName.Text, tbxPwd.Text);
            cbxDatabase.DisplayMember = "name";
        }

        private void cbxDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            //loadDBName();
        }

        private void cbxDatabase_DropDown(object sender, EventArgs e)
        {
            loadDBName();
        }

        private void btnSaveCfg_Click(object sender, EventArgs e)
        {
            CauHinh.SaveConfig(cbxServerName.Text, tbxUserName.Text, tbxPwd.Text, cbxDatabase.Text);
            this.Close();
        }

        private void btnCancelCfg_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
