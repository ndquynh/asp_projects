﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFormWithDB
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxPwd.Text.Trim()))
            {
                MessageBox.Show("Không được bỏ trống" + label1.Text.ToLower());
                this.tbxPwd.Focus();
                return;
            }
            if (string.IsNullOrEmpty(this.tbxUserName.Text))
            {
                MessageBox.Show("Không được bỏ trống" + label2.Text.ToLower());
                this.tbxUserName.Focus();
                return;
            }
            int kq = CauHinh.Check_Config(); //hàm Check_Config() thuộc Class QL_NguoiDung
            if (kq == 0)
            {
                ProcessLogin();// Cấu hình phù hợp xử lý đăng nhập
            }
            if (kq == 1)
            {
                MessageBox.Show("Chuỗi cấu hình không tồn tại");// Xử lý cấu hình
                ProcessConfig();
            }
            if (kq == 2)
            {
                MessageBox.Show("Chuỗi cấu hình không phù hợp");// Xử lý cấu hình
                ProcessConfig();
            }
        }

        private void ProcessConfig()
        {
            new ServerConfigForm().ShowDialog();
        }

        private void ProcessLogin()
        {
            LoginResult result;
            result = CauHinh.Check_User(tbxUserName.Text, tbxPwd.Text); //
            //Check_User viết trong Class QL_NguoiDung
            // Wrong username or pass
            if (result == LoginResult.Invalid)
            {
                MessageBox.Show("Sai " + label1.Text + " Hoặc " + label2.Text);
                return;
            }
            // Account had been disabled
            else if (result == LoginResult.Disabled)
            {
                MessageBox.Show("Tài khoản bị khóa");
                return;
            }
            if (Program.mainWindow == null || Program.mainWindow.IsDisposed)
            {
                Program.mainWindow = new MainWindow();
            }
            this.Visible = false;
            Program.mainWindow.Show();
        }
    }
}
