﻿using UpperText;

namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.emailTextBox1 = new EmailTextBox.EmailTextBox();
            this.upperTextBox2 = new UpperText.UpperTextBox();
            this.nonSpecialLetterTextBox1 = new NoneSpecialChar.NonSpecialLetterTextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Upper Case Text Box";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(491, 96);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "submit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Email Text box";
            // 
            // emailTextBox1
            // 
            this.emailTextBox1.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.emailTextBox1.Content = "";
            this.emailTextBox1.Location = new System.Drawing.Point(174, 99);
            this.emailTextBox1.Name = "emailTextBox1";
            this.emailTextBox1.Size = new System.Drawing.Size(311, 20);
            this.emailTextBox1.TabIndex = 2;
            // 
            // upperTextBox2
            // 
            this.upperTextBox2.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.upperTextBox2.Location = new System.Drawing.Point(174, 44);
            this.upperTextBox2.Name = "upperTextBox2";
            this.upperTextBox2.Size = new System.Drawing.Size(311, 20);
            this.upperTextBox2.TabIndex = 5;
            // 
            // nonSpecialLetterTextBox1
            // 
            this.nonSpecialLetterTextBox1.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.nonSpecialLetterTextBox1.Location = new System.Drawing.Point(174, 147);
            this.nonSpecialLetterTextBox1.Name = "nonSpecialLetterTextBox1";
            this.nonSpecialLetterTextBox1.Size = new System.Drawing.Size(311, 20);
            this.nonSpecialLetterTextBox1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Non Special Character Textbox";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nonSpecialLetterTextBox1);
            this.Controls.Add(this.upperTextBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.emailTextBox1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UpperText.UpperTextBox upperTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private EmailTextBox.EmailTextBox emailTextBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private UpperTextBox upperTextBox2;
        private NoneSpecialChar.NonSpecialLetterTextBox nonSpecialLetterTextBox1;
        private System.Windows.Forms.Label label3;
    }
}

