﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (emailTextBox1?.ValidationValue?.ErrorCode == EmailTextBox.ValidationValue.Valid)
            {
                errorProvider1.SetError(emailTextBox1, "");
            }
            else
            {
                errorProvider1.SetError(emailTextBox1, emailTextBox1.ValidationValue.ErrorMessage);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
