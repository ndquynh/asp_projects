﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoneSpecialChar
{
    public class ValidationValue
    {
        public const int Valid = 0;
        public const int Invalid = 1;

        public ValidationValue(int errorCode, String errorMessage)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
        }

        public int ErrorCode { get; set; }

        public String ErrorMessage { get; set; }
    }
}
