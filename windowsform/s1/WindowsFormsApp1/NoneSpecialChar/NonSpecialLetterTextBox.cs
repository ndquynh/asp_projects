﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoneSpecialChar
{
    public partial class NonSpecialLetterTextBox : TextBox
    {
        public AutoScaleMode AutoScaleMode { get; set; }
        public NonSpecialLetterTextBox()
        {
            InitializeComponent();
            //this.TextChanged += new EventHandler(OnContentChanged);
            this.KeyPress += new KeyPressEventHandler(NewKeyPressHandler);
        }

        private void NewKeyPressHandler(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetterOrDigit(e.KeyChar))
            {
                e.Handled = true;
                ValidationValue = new ValidationValue(1, "Text box khong duoc chua ky tu dac biet");
            }
            else
            {
                ValidationValue = new ValidationValue(0, "");
            }
        }

        //private String _content = String.Empty;

        private ValidationValue _validationValue = new ValidationValue(0, "");

        public ValidationValue ValidationValue
        {
            get { return _validationValue; }
            set
            {
                _validationValue = value;
                NotifyPropertyChanged();
            }
        }

        //public String Content
        //{
        //    get => _content;
        //    set
        //    {
        //        if (value != this._content)
        //        {
        //            _content = value;
        //            NotifyPropertyChanged();
        //        }
        //    }
        //}

        
        /**
         * Set Text value for Content property
         */
        //private void OnContentChanged(object sender, EventArgs e)
        //{
        //    Content = this.Text;

        //    if (!Regex.IsMatch(this.Content, @"[A-Za-z0-9]+@[A-Za-z0-9]+\.com"))
        //    {
        //        ValidationValue = new ValidationValue(1, "Email phai chua ky tu @ va ket thuc bang .com");
        //    }
        //    else
        //    {
        //        ValidationValue = new ValidationValue(0, "");
        //    }

        //}

        /**
         * This is binding technique
         */
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}
