﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UpperText
{
    public partial class UpperTextBox : TextBox
    {
        public AutoScaleMode AutoScaleMode { get; set; }

        public UpperTextBox()
        {
            InitializeComponent();
            this.KeyPress += new KeyPressEventHandler(NewKeyPressHandler);
        }

        private void NewKeyPressHandler(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar)) {
                e.Handled = true;
            }
        }
    }
}
