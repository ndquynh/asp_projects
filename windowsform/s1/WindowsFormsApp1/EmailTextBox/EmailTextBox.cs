﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmailTextBox
{
    public partial class EmailTextBox : TextBox, INotifyPropertyChanged
    {
        public AutoScaleMode AutoScaleMode { get; set; }

        private String _content = String.Empty;

        private ValidationValue _validationValue = new ValidationValue(0, "");

        public ValidationValue ValidationValue
        {
            get { return _validationValue; }
            set
            {
                _validationValue = value;
                NotifyPropertyChanged();
            }
        }

        public String Content
        {
            get => _content;
            set
            {
                if (value != this._content)
                {
                    _content = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public EmailTextBox()
        {
            InitializeComponent();
            //this.Validating += new CancelEventHandler(ValidateContent);
            this.TextChanged += new EventHandler(OnContentChanged);
        }

        /**
         * Set Text value for Content property
         */
        private void OnContentChanged(object sender, EventArgs e)
        {
            Content = this.Text;

            if (!Regex.IsMatch(this.Content, @"[A-Za-z0-9]+@[A-Za-z0-9]+\.com"))
            {
                ValidationValue = new ValidationValue(1, "Email phai chua ky tu @ va ket thuc bang .com");
            }
            else
            {
                ValidationValue = new ValidationValue(0, "");
            }
            
        }

        /**
         * This is binding technique
         */
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
 
        
    }

}
