﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSV
{
    public partial class FormQLSinhVien : Form
    {
        QLSVDataContext qlsv = new QLSVDataContext();

        public FormQLSinhVien()
        {
            InitializeComponent();
        }

        private void FormQLSinhVien_Load(object sender, EventArgs e)
        {
            var svs = from sv in qlsv.SinhViens select sv;

            dataGridViewSV.DataSource = svs;

            var classes = from cls in qlsv.Lops select cls;

            cbxLop.DataSource = classes;
            //cbxLop.DisplayMember = "";
        }

        private void tool_btnAdd_Click(object sender, EventArgs e)
        {

        }

        private void tool_btnEdit_Click(object sender, EventArgs e)
        {

        }

        private void tool_btnRemove_Click(object sender, EventArgs e)
        {

        }

        private void tool_btnSave_Click(object sender, EventArgs e)
        {

        }

        private void tool_btnClose_Click(object sender, EventArgs e)
        {

        }

      
    }
}
