﻿namespace QLSV
{
    partial class FormQuanLyKhoa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormQuanLyKhoa));
            this.label1 = new System.Windows.Forms.Label();
            this.tbxMaKhoa = new System.Windows.Forms.TextBox();
            this.tbxTenKhoa = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewKhoa = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tool_btnAdd = new System.Windows.Forms.ToolStripButton();
            this.tool_btnEdit = new System.Windows.Forms.ToolStripButton();
            this.tool_btnRemove = new System.Windows.Forms.ToolStripButton();
            this.tool_btnSave = new System.Windows.Forms.ToolStripButton();
            this.tool_btnClose = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewKhoa)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(102, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ma Khoa";
            // 
            // tbxMaKhoa
            // 
            this.tbxMaKhoa.Location = new System.Drawing.Point(183, 34);
            this.tbxMaKhoa.Name = "tbxMaKhoa";
            this.tbxMaKhoa.Size = new System.Drawing.Size(274, 20);
            this.tbxMaKhoa.TabIndex = 1;
            this.tbxMaKhoa.TextChanged += new System.EventHandler(this.tbxMaKhoa_TextChanged);
            // 
            // tbxTenKhoa
            // 
            this.tbxTenKhoa.Location = new System.Drawing.Point(183, 69);
            this.tbxTenKhoa.Name = "tbxTenKhoa";
            this.tbxTenKhoa.Size = new System.Drawing.Size(274, 20);
            this.tbxTenKhoa.TabIndex = 3;
            this.tbxTenKhoa.TextChanged += new System.EventHandler(this.tbxTenKhoa_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ten Khoa";
            // 
            // dataGridViewKhoa
            // 
            this.dataGridViewKhoa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewKhoa.Location = new System.Drawing.Point(12, 110);
            this.dataGridViewKhoa.Name = "dataGridViewKhoa";
            this.dataGridViewKhoa.Size = new System.Drawing.Size(669, 209);
            this.dataGridViewKhoa.TabIndex = 4;
            this.dataGridViewKhoa.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewKhoa_CellClick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tool_btnAdd,
            this.tool_btnEdit,
            this.tool_btnRemove,
            this.tool_btnSave,
            this.tool_btnClose});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(693, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tool_btnAdd
            // 
            this.tool_btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("tool_btnAdd.Image")));
            this.tool_btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_btnAdd.Name = "tool_btnAdd";
            this.tool_btnAdd.Size = new System.Drawing.Size(49, 22);
            this.tool_btnAdd.Text = "Add";
            this.tool_btnAdd.Click += new System.EventHandler(this.tool_btnAdd_Click);
            // 
            // tool_btnEdit
            // 
            this.tool_btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("tool_btnEdit.Image")));
            this.tool_btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_btnEdit.Name = "tool_btnEdit";
            this.tool_btnEdit.Size = new System.Drawing.Size(47, 22);
            this.tool_btnEdit.Text = "Edit";
            this.tool_btnEdit.Click += new System.EventHandler(this.tool_btnEdit_Click);
            // 
            // tool_btnRemove
            // 
            this.tool_btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("tool_btnRemove.Image")));
            this.tool_btnRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_btnRemove.Name = "tool_btnRemove";
            this.tool_btnRemove.Size = new System.Drawing.Size(70, 22);
            this.tool_btnRemove.Text = "Remove";
            this.tool_btnRemove.Click += new System.EventHandler(this.tool_btnRemove_Click);
            // 
            // tool_btnSave
            // 
            this.tool_btnSave.Image = ((System.Drawing.Image)(resources.GetObject("tool_btnSave.Image")));
            this.tool_btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_btnSave.Name = "tool_btnSave";
            this.tool_btnSave.Size = new System.Drawing.Size(51, 22);
            this.tool_btnSave.Text = "Save";
            this.tool_btnSave.Click += new System.EventHandler(this.tool_btnSave_Click);
            // 
            // tool_btnClose
            // 
            this.tool_btnClose.Image = ((System.Drawing.Image)(resources.GetObject("tool_btnClose.Image")));
            this.tool_btnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_btnClose.Name = "tool_btnClose";
            this.tool_btnClose.Size = new System.Drawing.Size(56, 22);
            this.tool_btnClose.Text = "Close";
            this.tool_btnClose.Click += new System.EventHandler(this.tool_btnClose_Click);
            // 
            // FormQuanLyKhoa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 331);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridViewKhoa);
            this.Controls.Add(this.tbxTenKhoa);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxMaKhoa);
            this.Controls.Add(this.label1);
            this.Name = "FormQuanLyKhoa";
            this.Text = "FormQuanLyKhoa";
            this.Load += new System.EventHandler(this.FormQuanLyKhoa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewKhoa)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxMaKhoa;
        private System.Windows.Forms.TextBox tbxTenKhoa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridViewKhoa;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tool_btnAdd;
        private System.Windows.Forms.ToolStripButton tool_btnEdit;
        private System.Windows.Forms.ToolStripButton tool_btnRemove;
        private System.Windows.Forms.ToolStripButton tool_btnSave;
        private System.Windows.Forms.ToolStripButton tool_btnClose;
    }
}