﻿namespace LoginFormWithDB
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nguoiDungToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nhomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.themNguoiDungToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.themNguoiDungVaoNhomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quyenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phanQuyenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manHinhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nguoiDungToolStripMenuItem,
            this.quyenToolStripMenuItem,
            this.manHinhToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(831, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // nguoiDungToolStripMenuItem
            // 
            this.nguoiDungToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nhomToolStripMenuItem,
            this.themNguoiDungToolStripMenuItem,
            this.themNguoiDungVaoNhomToolStripMenuItem});
            this.nguoiDungToolStripMenuItem.Name = "nguoiDungToolStripMenuItem";
            this.nguoiDungToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.nguoiDungToolStripMenuItem.Text = "Người Dùng";
            this.nguoiDungToolStripMenuItem.Click += new System.EventHandler(this.NguoiDungToolStripMenuItem_Click);
            // 
            // nhomToolStripMenuItem
            // 
            this.nhomToolStripMenuItem.Name = "nhomToolStripMenuItem";
            this.nhomToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.nhomToolStripMenuItem.Text = "Nhóm Người Dùng";
            this.nhomToolStripMenuItem.Click += new System.EventHandler(this.nhomToolStripMenuItem_Click);
            // 
            // themNguoiDungToolStripMenuItem
            // 
            this.themNguoiDungToolStripMenuItem.Name = "themNguoiDungToolStripMenuItem";
            this.themNguoiDungToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.themNguoiDungToolStripMenuItem.Text = "Thêm Người dùng";
            this.themNguoiDungToolStripMenuItem.Click += new System.EventHandler(this.ThemNguoiDungToolStripMenuItem_Click);
            // 
            // themNguoiDungVaoNhomToolStripMenuItem
            // 
            this.themNguoiDungVaoNhomToolStripMenuItem.Name = "themNguoiDungVaoNhomToolStripMenuItem";
            this.themNguoiDungVaoNhomToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.themNguoiDungVaoNhomToolStripMenuItem.Text = "Thêm Người Dùng Vào Nhóm";
            this.themNguoiDungVaoNhomToolStripMenuItem.Click += new System.EventHandler(this.themNguoiDungVaoNhomToolStripMenuItem_Click);
            // 
            // quyenToolStripMenuItem
            // 
            this.quyenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.phanQuyenToolStripMenuItem});
            this.quyenToolStripMenuItem.Name = "quyenToolStripMenuItem";
            this.quyenToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.quyenToolStripMenuItem.Text = "Quyền";
            // 
            // phanQuyenToolStripMenuItem
            // 
            this.phanQuyenToolStripMenuItem.Name = "phanQuyenToolStripMenuItem";
            this.phanQuyenToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.phanQuyenToolStripMenuItem.Text = "Phân Quyền";
            this.phanQuyenToolStripMenuItem.Click += new System.EventHandler(this.phanQuyenToolStripMenuItem_Click);
            // 
            // manHinhToolStripMenuItem
            // 
            this.manHinhToolStripMenuItem.Name = "manHinhToolStripMenuItem";
            this.manHinhToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.manHinhToolStripMenuItem.Text = "Màn Hình";
            this.manHinhToolStripMenuItem.Click += new System.EventHandler(this.manHinhToolStripMenuItem_Click);
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.loginToolStripMenuItem.Text = "Login";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 416);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "Quan Ly Nguoi Dung";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nguoiDungToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem themNguoiDungToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nhomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quyenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phanQuyenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem themNguoiDungVaoNhomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manHinhToolStripMenuItem;
    }
}

