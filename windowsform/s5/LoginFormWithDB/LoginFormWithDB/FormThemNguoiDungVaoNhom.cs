﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFormWithDB
{
    public partial class FormThemNguoiDungVaoNhom : Form
    {
        public FormThemNguoiDungVaoNhom()
        {
            InitializeComponent();
        }

        private void qL_NguoiDungBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.qL_NguoiDungBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSet1);

        }

        private void FormThemNguoiDungVaoNhom_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.QL_NhomNguoiDung' table. You can move, or remove it, as needed.
            this.qL_NhomNguoiDungTableAdapter.Fill(this.dataSet1.QL_NhomNguoiDung);
            // TODO: This line of code loads data into the 'dataSet1.QL_NguoiDung' table. You can move, or remove it, as needed.
            this.qL_NguoiDungTableAdapter.Fill(this.dataSet1.QL_NguoiDung);

        }


        private void qL_NhomNguoiDungComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadTableNguoiDungNhomNguoiDung();
        }

        private void loadTableNguoiDungNhomNguoiDung()
        {
            try
            {
                this.themNguoiDungVaoNhomTableAdapter.Fill_my(this.dataSet1.ThemNguoiDungVaoNhom, qL_NhomNguoiDungComboBox.SelectedValue.ToString());
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (qL_NguoiDungDataGridView.SelectedCells.Count == 0)
            {
                return;
            }

            int selectedrowindex = qL_NguoiDungDataGridView.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = qL_NguoiDungDataGridView.Rows[selectedrowindex];
            string userName = selectedRow.Cells[0].Value.ToString();

            string maNhom = qL_NhomNguoiDungComboBox.SelectedValue.ToString();

            //check user exist
            DataSet1.ThemNguoiDungVaoNhomDataTable checkExistResult = themNguoiDungVaoNhomTableAdapter.GetData_ToCheckExist(userName, maNhom);

            if(checkExistResult != null && checkExistResult.Rows.Count > 0)
            {
                MessageBox.Show("User " + userName + " da ton tai!", "Error");

            } else
            {
                //themNguoiDungVaoNhomBindingSource.Insert()
                themNguoiDungVaoNhomTableAdapter.InsertQuery_QL_NguoiDungNhomNguoiDung(userName, maNhom, userName + " " + maNhom);

                loadTableNguoiDungNhomNguoiDung();
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if(themNguoiDungVaoNhomDataGridView.SelectedCells.Count == 0)
            {
                return;
            }

            int selectedrowindex = themNguoiDungVaoNhomDataGridView.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = themNguoiDungVaoNhomDataGridView.Rows[selectedrowindex];
            string userName = selectedRow.Cells[0].Value.ToString();

            string maNhom = selectedRow.Cells[1].Value.ToString();

            //check user exist

            //themNguoiDungVaoNhomBindingSource.Insert()
            themNguoiDungVaoNhomTableAdapter.DeleteQuery_NguoiDungNhomNguoiDung(userName, maNhom);

            loadTableNguoiDungNhomNguoiDung();

        }


    }
}
