﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginFormWithDB
{
    public abstract class ApplicationSettingsBaseEx : global::System.Configuration.ApplicationSettingsBase
    {

        public SettingsPropertyCollection Properties { get; set; }
    }
}
