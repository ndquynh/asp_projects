﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFormWithDB
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void nhomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormNhomNguoiDung().ShowDialog(this);
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

        }

        private void ThemNguoiDungToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormUser().Show(this);
        }

        private void themNguoiDungVaoNhomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormThemNguoiDungVaoNhom().ShowDialog(this);

        }

        public void NguoiDungToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void manHinhToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormManHinh().Show(this);
        }

        private void phanQuyenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormPhanQuyen().Show(this);
        }
    }

    
}
