﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFormWithDB
{
    public partial class ServerConfigForm : Form
    {
        public ServerConfigForm()
        {
            InitializeComponent();

            //ThreadStart childref = new ThreadStart(loadServerName);
            //Console.WriteLine("In Main: Creating the Child thread");
            //Thread childThread = new Thread(childref);
            //childThread.Start();
        }

        private void ServerConfigForm_Load(object sender, EventArgs e)
        {
            loadServerName();

        }
        async Task loadServerName()
        {
            var abc =  (CauHinh.GetServerNameAsync());

            // cbxServerName.DataSource =
            cbxServerName.DataSource = abc;
            cbxServerName.DisplayMember = "ServerName";
            return ;
        }
        
        private void cbxServerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ThreadStart childref = new ThreadStart(loadDBName);
            //Console.WriteLine("In Main: Creating the Child thread");
            //Thread childThread = new Thread(childref);
            //childThread.Start();
            
        }

        void loadDBName()
        {
            cbxDatabase.DataSource = CauHinh.GetDBName(cbxServerName.Text, tbxUserName.Text, tbxPwd.Text);
            cbxDatabase.DisplayMember = "name";
        }

        private void cbxDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            //loadDBName();
        }

        private void cbxDatabase_DropDown(object sender, EventArgs e)
        {
            loadDBName();
        }

        private void btnSaveCfg_Click(object sender, EventArgs e)
        {
            CauHinh.SaveConfig(cbxServerName.Text, cbxDatabase.Text, tbxUserName.Text, pPass: tbxPwd.Text);
            this.Close();
        }

        private void btnCancelCfg_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ServerConfigForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            SaveConfigByEnter(e);
        }

        private void SaveConfigByEnter(KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                //CauHinh.SaveConfig(cbxServerName.Text, tbxUserName.Text, tbxPwd.Text, cbxDatabase.Text);
                this.Close();
                e.Handled = true;

            }
        }

        private void cbxServerName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!cbxServerName.DroppedDown && cbxDatabase.DroppedDown)
            {
                SaveConfigByEnter(e);
            }
        }
    }
}
