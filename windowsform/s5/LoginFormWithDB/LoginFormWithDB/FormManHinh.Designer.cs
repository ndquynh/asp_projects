﻿namespace LoginFormWithDB
{
    partial class FormManHinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormManHinh));
            System.Windows.Forms.Label maManHinhLabel;
            System.Windows.Forms.Label maNhomNguoiDungLabel;
            this.dataSet1 = new LoginFormWithDB.DataSet1();
            this.qL_PhanQuyenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.qL_PhanQuyenTableAdapter = new LoginFormWithDB.DataSet1TableAdapters.QL_PhanQuyenTableAdapter();
            this.tableAdapterManager = new LoginFormWithDB.DataSet1TableAdapters.TableAdapterManager();
            this.qL_PhanQuyenBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.qL_PhanQuyenBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.maManHinhTextBox = new System.Windows.Forms.TextBox();
            this.maNhomNguoiDungTextBox = new System.Windows.Forms.TextBox();
            this.qL_PhanQuyenDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            maManHinhLabel = new System.Windows.Forms.Label();
            maNhomNguoiDungLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qL_PhanQuyenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qL_PhanQuyenBindingNavigator)).BeginInit();
            this.qL_PhanQuyenBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qL_PhanQuyenDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // qL_PhanQuyenBindingSource
            // 
            this.qL_PhanQuyenBindingSource.DataMember = "QL_PhanQuyen";
            this.qL_PhanQuyenBindingSource.DataSource = this.dataSet1;
            // 
            // qL_PhanQuyenTableAdapter
            // 
            this.qL_PhanQuyenTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.QL_NguoiDungNhomNguoiDungTableAdapter = null;
            this.tableAdapterManager.QL_NguoiDungTableAdapter = null;
            this.tableAdapterManager.QL_NhomNguoiDungTableAdapter = null;
            this.tableAdapterManager.QL_PhanQuyenTableAdapter = this.qL_PhanQuyenTableAdapter;
            this.tableAdapterManager.UpdateOrder = LoginFormWithDB.DataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // qL_PhanQuyenBindingNavigator
            // 
            this.qL_PhanQuyenBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.qL_PhanQuyenBindingNavigator.BindingSource = this.qL_PhanQuyenBindingSource;
            this.qL_PhanQuyenBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.qL_PhanQuyenBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.qL_PhanQuyenBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.qL_PhanQuyenBindingNavigatorSaveItem});
            this.qL_PhanQuyenBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.qL_PhanQuyenBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.qL_PhanQuyenBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.qL_PhanQuyenBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.qL_PhanQuyenBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.qL_PhanQuyenBindingNavigator.Name = "qL_PhanQuyenBindingNavigator";
            this.qL_PhanQuyenBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.qL_PhanQuyenBindingNavigator.Size = new System.Drawing.Size(452, 25);
            this.qL_PhanQuyenBindingNavigator.TabIndex = 0;
            this.qL_PhanQuyenBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 15);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // qL_PhanQuyenBindingNavigatorSaveItem
            // 
            this.qL_PhanQuyenBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.qL_PhanQuyenBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("qL_PhanQuyenBindingNavigatorSaveItem.Image")));
            this.qL_PhanQuyenBindingNavigatorSaveItem.Name = "qL_PhanQuyenBindingNavigatorSaveItem";
            this.qL_PhanQuyenBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 23);
            this.qL_PhanQuyenBindingNavigatorSaveItem.Text = "Save Data";
            this.qL_PhanQuyenBindingNavigatorSaveItem.Click += new System.EventHandler(this.qL_PhanQuyenBindingNavigatorSaveItem_Click);
            // 
            // maManHinhLabel
            // 
            maManHinhLabel.AutoSize = true;
            maManHinhLabel.Location = new System.Drawing.Point(61, 31);
            maManHinhLabel.Name = "maManHinhLabel";
            maManHinhLabel.Size = new System.Drawing.Size(74, 13);
            maManHinhLabel.TabIndex = 1;
            maManHinhLabel.Text = "Ma Man Hinh:";
            // 
            // maManHinhTextBox
            // 
            this.maManHinhTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.qL_PhanQuyenBindingSource, "MaManHinh", true));
            this.maManHinhTextBox.Location = new System.Drawing.Point(141, 28);
            this.maManHinhTextBox.Name = "maManHinhTextBox";
            this.maManHinhTextBox.Size = new System.Drawing.Size(100, 20);
            this.maManHinhTextBox.TabIndex = 2;
            // 
            // maNhomNguoiDungLabel
            // 
            maNhomNguoiDungLabel.AutoSize = true;
            maNhomNguoiDungLabel.Location = new System.Drawing.Point(19, 57);
            maNhomNguoiDungLabel.Name = "maNhomNguoiDungLabel";
            maNhomNguoiDungLabel.Size = new System.Drawing.Size(116, 13);
            maNhomNguoiDungLabel.TabIndex = 3;
            maNhomNguoiDungLabel.Text = "Ma Nhom Nguoi Dung:";
            // 
            // maNhomNguoiDungTextBox
            // 
            this.maNhomNguoiDungTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.qL_PhanQuyenBindingSource, "MaNhomNguoiDung", true));
            this.maNhomNguoiDungTextBox.Location = new System.Drawing.Point(141, 54);
            this.maNhomNguoiDungTextBox.Name = "maNhomNguoiDungTextBox";
            this.maNhomNguoiDungTextBox.Size = new System.Drawing.Size(100, 20);
            this.maNhomNguoiDungTextBox.TabIndex = 4;
            // 
            // qL_PhanQuyenDataGridView
            // 
            this.qL_PhanQuyenDataGridView.AutoGenerateColumns = false;
            this.qL_PhanQuyenDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.qL_PhanQuyenDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewCheckBoxColumn1});
            this.qL_PhanQuyenDataGridView.DataSource = this.qL_PhanQuyenBindingSource;
            this.qL_PhanQuyenDataGridView.Location = new System.Drawing.Point(12, 132);
            this.qL_PhanQuyenDataGridView.Name = "qL_PhanQuyenDataGridView";
            this.qL_PhanQuyenDataGridView.Size = new System.Drawing.Size(425, 228);
            this.qL_PhanQuyenDataGridView.TabIndex = 5;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "MaNhomNguoiDung";
            this.dataGridViewTextBoxColumn1.HeaderText = "MaNhomNguoiDung";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "MaManHinh";
            this.dataGridViewTextBoxColumn2.HeaderText = "MaManHinh";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "CoQuyen";
            this.dataGridViewCheckBoxColumn1.HeaderText = "CoQuyen";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // FormManHinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 372);
            this.Controls.Add(this.qL_PhanQuyenDataGridView);
            this.Controls.Add(maNhomNguoiDungLabel);
            this.Controls.Add(this.maNhomNguoiDungTextBox);
            this.Controls.Add(maManHinhLabel);
            this.Controls.Add(this.maManHinhTextBox);
            this.Controls.Add(this.qL_PhanQuyenBindingNavigator);
            this.Name = "FormManHinh";
            this.Text = "FormManHinh";
            this.Load += new System.EventHandler(this.FormManHinh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qL_PhanQuyenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qL_PhanQuyenBindingNavigator)).EndInit();
            this.qL_PhanQuyenBindingNavigator.ResumeLayout(false);
            this.qL_PhanQuyenBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qL_PhanQuyenDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource qL_PhanQuyenBindingSource;
        private DataSet1TableAdapters.QL_PhanQuyenTableAdapter qL_PhanQuyenTableAdapter;
        private DataSet1TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator qL_PhanQuyenBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton qL_PhanQuyenBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox maManHinhTextBox;
        private System.Windows.Forms.TextBox maNhomNguoiDungTextBox;
        private System.Windows.Forms.DataGridView qL_PhanQuyenDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
    }
}