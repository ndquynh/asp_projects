﻿namespace LoginFormWithDB
{
    partial class ServerConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxUserName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSaveCfg = new System.Windows.Forms.Button();
            this.btnCancelCfg = new System.Windows.Forms.Button();
            this.tbxPwd = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxDatabase = new System.Windows.Forms.ComboBox();
            this.cbxServerName = new System.Windows.Forms.ComboBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.SuspendLayout();
            // 
            // tbxUserName
            // 
            this.tbxUserName.Location = new System.Drawing.Point(113, 55);
            this.tbxUserName.Name = "tbxUserName";
            this.tbxUserName.Size = new System.Drawing.Size(267, 20);
            this.tbxUserName.TabIndex = 1;
            this.tbxUserName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxServerName_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "User Name:";
            // 
            // btnSaveCfg
            // 
            this.btnSaveCfg.Location = new System.Drawing.Point(224, 151);
            this.btnSaveCfg.Name = "btnSaveCfg";
            this.btnSaveCfg.Size = new System.Drawing.Size(75, 23);
            this.btnSaveCfg.TabIndex = 4;
            this.btnSaveCfg.Text = "Save Config";
            this.btnSaveCfg.UseVisualStyleBackColor = true;
            this.btnSaveCfg.Click += new System.EventHandler(this.btnSaveCfg_Click);
            // 
            // btnCancelCfg
            // 
            this.btnCancelCfg.Location = new System.Drawing.Point(305, 151);
            this.btnCancelCfg.Name = "btnCancelCfg";
            this.btnCancelCfg.Size = new System.Drawing.Size(75, 23);
            this.btnCancelCfg.TabIndex = 5;
            this.btnCancelCfg.Text = "Cancel";
            this.btnCancelCfg.UseVisualStyleBackColor = true;
            this.btnCancelCfg.Click += new System.EventHandler(this.btnCancelCfg_Click);
            // 
            // tbxPwd
            // 
            this.tbxPwd.Location = new System.Drawing.Point(113, 81);
            this.tbxPwd.Name = "tbxPwd";
            this.tbxPwd.Size = new System.Drawing.Size(267, 20);
            this.tbxPwd.TabIndex = 2;
            this.tbxPwd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxServerName_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Password:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Database:";
            // 
            // cbxDatabase
            // 
            this.cbxDatabase.FormattingEnabled = true;
            this.cbxDatabase.Location = new System.Drawing.Point(113, 107);
            this.cbxDatabase.Name = "cbxDatabase";
            this.cbxDatabase.Size = new System.Drawing.Size(267, 21);
            this.cbxDatabase.TabIndex = 3;
            this.cbxDatabase.DropDown += new System.EventHandler(this.cbxDatabase_DropDown);
            this.cbxDatabase.SelectedIndexChanged += new System.EventHandler(this.cbxDatabase_SelectedIndexChanged);
            this.cbxDatabase.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxServerName_KeyPress);
            // 
            // cbxServerName
            // 
            this.cbxServerName.FormattingEnabled = true;
            this.cbxServerName.Location = new System.Drawing.Point(113, 28);
            this.cbxServerName.Name = "cbxServerName";
            this.cbxServerName.Size = new System.Drawing.Size(267, 21);
            this.cbxServerName.TabIndex = 0;
            this.cbxServerName.SelectedIndexChanged += new System.EventHandler(this.cbxServerName_SelectedIndexChanged);
            this.cbxServerName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxServerName_KeyPress);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(12, 31);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 23);
            this.ultraLabel1.TabIndex = 6;
            this.ultraLabel1.Text = "Server Name:";
            // 
            // ServerConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 186);
            this.ControlBox = false;
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.cbxServerName);
            this.Controls.Add(this.cbxDatabase);
            this.Controls.Add(this.btnCancelCfg);
            this.Controls.Add(this.btnSaveCfg);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxPwd);
            this.Controls.Add(this.tbxUserName);
            this.Name = "ServerConfigForm";
            this.Text = "Connection Configuation";
            this.Load += new System.EventHandler(this.ServerConfigForm_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ServerConfigForm_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tbxUserName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSaveCfg;
        private System.Windows.Forms.Button btnCancelCfg;
        private System.Windows.Forms.TextBox tbxPwd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbxDatabase;
        private System.Windows.Forms.ComboBox cbxServerName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
    }
}