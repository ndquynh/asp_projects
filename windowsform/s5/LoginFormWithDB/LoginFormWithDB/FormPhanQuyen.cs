﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginFormWithDB
{
    public partial class FormPhanQuyen : Form
    {
        public FormPhanQuyen()
        {
            InitializeComponent();
        }

        private void qL_PhanQuyenBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.qL_PhanQuyenBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSet1);

        }

        private void FormPhanQuyen_Load(object sender, EventArgs e)
        {
            //this.dataSet1.QL_GET_PhanQuyen.Columns["TenManHinh"].SetOrdinal(1);
            //this.dataSet1.QL_GET_PhanQuyen.Columns["CoQuyen"].SetOrdinal(2);

            
            // TODO: This line of code loads data into the 'dataSet1.QL_NhomNguoiDung' table. You can move, or remove it, as needed.
            this.qL_NhomNguoiDungTableAdapter.Fill(this.dataSet1.QL_NhomNguoiDung);
            // TODO: This line of code loads data into the 'dataSet1.QL_PhanQuyen' table. You can move, or remove it, as needed.
            
            this.qL_PhanQuyenTableAdapter.Fill(this.dataSet1.QL_PhanQuyen);

            string[] columnNames = { "MaNhomNguoiDung", "MaManHinh", "CoQuyen" };
            //List<string> listColNames = columnNames.ToList();
            //foreach (string colName in columnNames)
            //{
            //    this.dataSet1.QL_GET_PhanQuyen.Columns[colName].SetOrdinal(listColNames.IndexOf(colName));
            //}
            //this.dataSet1.QL_GET_PhanQuyen.
        }

        private void LoadDataUponCondition()
        {

            int selectedrowindex = qL_NhomNguoiDungDataGridView.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = qL_NhomNguoiDungDataGridView.Rows[selectedrowindex];
            string groupId = selectedRow.Cells[0].Value.ToString();

            try
            {
                
                this.qL_GET_PhanQuyenTableAdapter.Fill(this.dataSet1.QL_GET_PhanQuyen, groupId);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void qL_NhomNguoiDungDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            LoadDataUponCondition();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (qL_NhomNguoiDungDataGridView.CurrentRow == null)
            {
                return;
            }

            string _nhomUser = qL_NhomNguoiDungDataGridView.CurrentRow.Cells[0].Value.ToString();

           for(int rowIndex = 0; rowIndex < qL_GET_PhanQuyenDataGridView.Rows.Count - 1; rowIndex++ )
            {
                DataGridViewRow item = qL_GET_PhanQuyenDataGridView.Rows[rowIndex];

                if (qL_GET_PhanQuyenTableAdapter.KiemTraKhoaChinhPhanQuyen(_nhomUser, item.Cells[0].Value.ToString()) == 0)
                {
                    try
                    {
                        qL_PhanQuyenTableAdapter.Insert(_nhomUser, item.Cells[0].Value.ToString(), (bool)(item.Cells[1].Value));
                    }
                    catch {
                        qL_PhanQuyenTableAdapter.Insert(_nhomUser, item.Cells[0].Value.ToString(), false);
                    }
                }
                else
                {
                    qL_PhanQuyenTableAdapter.UpdateQuery((item.Cells[1] == null) ? false : (bool)(item.Cells[1].Value), _nhomUser, item.Cells[0].Value.ToString());
                }
            }
        }

        //qL_PhanQuyenTableAdapter.UpdateQuery()
    }

}
