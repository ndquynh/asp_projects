﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class B : A
    {
        public new virtual void Test()
        {
            Console.WriteLine("B::Test()");
        }
    }
}
