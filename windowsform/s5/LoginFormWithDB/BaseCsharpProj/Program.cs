using Polymorphism;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseCsharpProj
{
    class Program
    {
        static void Main(string[] args)
        {
            A a = new A();
            B b = new B();
            C c = new C();

            Console.Write("a.Test() => "); a.Test(); Console.WriteLine();

            Console.Write("b.Test() => "); b.Test(); Console.WriteLine();

            Console.Write("c.Test() => "); c.Test(); Console.WriteLine();

            a = new B();
            Console.Write("a = new B(); a.Test() => "); a.Test(); Console.WriteLine();

            b = new C();
            Console.Write("b = new C(); b.Test() => "); b.Test(); Console.WriteLine();




            //b = (B)new A();
            //Console.Write("b = (B)new A(); b.Test() => "); b.Test(); Console.WriteLine();

            //c = (C)new B();
            //Console.Write("c = (C)new B(); c.Test() => "); c.Test(); Console.WriteLine();


            Console.ReadKey();
        }
    }
}
