USE [master]
GO
/****** Object:  Database [1.LTWNC]    Script Date: 11/09/2018 8:32:16 PM ******/
CREATE DATABASE [1.LTWNC]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'1.LTWNC', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\1.LTWNC.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'1.LTWNC_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\1.LTWNC_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [1.LTWNC] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [1.LTWNC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [1.LTWNC] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [1.LTWNC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [1.LTWNC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [1.LTWNC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [1.LTWNC] SET ARITHABORT OFF 
GO
ALTER DATABASE [1.LTWNC] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [1.LTWNC] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [1.LTWNC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [1.LTWNC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [1.LTWNC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [1.LTWNC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [1.LTWNC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [1.LTWNC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [1.LTWNC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [1.LTWNC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [1.LTWNC] SET  DISABLE_BROKER 
GO
ALTER DATABASE [1.LTWNC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [1.LTWNC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [1.LTWNC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [1.LTWNC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [1.LTWNC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [1.LTWNC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [1.LTWNC] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [1.LTWNC] SET RECOVERY FULL 
GO
ALTER DATABASE [1.LTWNC] SET  MULTI_USER 
GO
ALTER DATABASE [1.LTWNC] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [1.LTWNC] SET DB_CHAINING OFF 
GO
ALTER DATABASE [1.LTWNC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [1.LTWNC] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'1.LTWNC', N'ON'
GO
USE [1.LTWNC]
GO
/****** Object:  StoredProcedure [dbo].[NewSelectCommand]    Script Date: 11/09/2018 8:32:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NewSelectCommand]
AS
	SET NOCOUNT ON;
SELECT        QL_NguoiDung.TenDangNhap, QL_NguoiDung.MatKhau
FROM            QL_NguoiDung INNER JOIN
                         QL_NguoiDungNhomNguoiDung ON QL_NguoiDung.TenDangNhap = QL_NguoiDungNhomNguoiDung.TenDangNhap


GO
/****** Object:  StoredProcedure [dbo].[ScalarQuery]    Script Date: 11/09/2018 8:32:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScalarQuery]
AS
	SET NOCOUNT ON;
SELECT        QL_NguoiDung.TenDangNhap, QL_NguoiDung.MatKhau
FROM            QL_NguoiDung INNER JOIN
                         QL_NguoiDungNhomNguoiDung ON QL_NguoiDung.TenDangNhap = QL_NguoiDungNhomNguoiDung.TenDangNhap
WHERE        (QL_NguoiDung.TenDangNhap = N'QL_NguoiDungNhomNguoiDung')


GO
/****** Object:  Table [dbo].[DM_ManHinh]    Script Date: 11/09/2018 8:32:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_ManHinh](
	[MaManHinh] [nvarchar](50) NOT NULL,
	[TenManHinh] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_DM_] PRIMARY KEY CLUSTERED 
(
	[MaManHinh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QL_NguoiDung]    Script Date: 11/09/2018 8:32:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QL_NguoiDung](
	[TenDangNhap] [nvarchar](50) NOT NULL,
	[MatKhau] [nvarchar](100) NULL,
	[HoatDong] [bit] NULL,
 CONSTRAINT [PK_QL_NguoiDung] PRIMARY KEY CLUSTERED 
(
	[TenDangNhap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QL_NguoiDungNhomNguoiDung]    Script Date: 11/09/2018 8:32:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QL_NguoiDungNhomNguoiDung](
	[TenDangNhap] [nvarchar](50) NOT NULL,
	[MaNhomNguoiDung] [varchar](20) NOT NULL,
	[GhiChu] [nvarchar](200) NULL,
 CONSTRAINT [PK_QL_NguoiDungNhomNguoiDung] PRIMARY KEY CLUSTERED 
(
	[TenDangNhap] ASC,
	[MaNhomNguoiDung] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QL_NhomNguoiDung]    Script Date: 11/09/2018 8:32:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QL_NhomNguoiDung](
	[MaNhom] [varchar](20) NOT NULL,
	[TenNhom] [nvarchar](50) NOT NULL,
	[GhiChu] [nvarchar](200) NULL,
 CONSTRAINT [PK_QL_NhomNguoiDung] PRIMARY KEY CLUSTERED 
(
	[MaNhom] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QL_PhanQuyen]    Script Date: 11/09/2018 8:32:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QL_PhanQuyen](
	[MaNhomNguoiDung] [nvarchar](20) NOT NULL,
	[MaManHinh] [nvarchar](50) NOT NULL,
	[CoQuyen] [bit] NOT NULL,
 CONSTRAINT [PK_QL_PhanQuyen] PRIMARY KEY CLUSTERED 
(
	[MaNhomNguoiDung] ASC,
	[MaManHinh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[QL_NguoiDung] ([TenDangNhap], [MatKhau], [HoatDong]) VALUES (N'admin', N'123456', 1)
INSERT [dbo].[QL_NguoiDung] ([TenDangNhap], [MatKhau], [HoatDong]) VALUES (N'staff1', N'1', 0)
INSERT [dbo].[QL_NguoiDung] ([TenDangNhap], [MatKhau], [HoatDong]) VALUES (N'staff2', N'1', 1)
INSERT [dbo].[QL_NguoiDung] ([TenDangNhap], [MatKhau], [HoatDong]) VALUES (N'user', N'1', 1)
INSERT [dbo].[QL_NguoiDung] ([TenDangNhap], [MatKhau], [HoatDong]) VALUES (N'user1', N'1', 1)
INSERT [dbo].[QL_NguoiDungNhomNguoiDung] ([TenDangNhap], [MaNhomNguoiDung], [GhiChu]) VALUES (N'admin', N'1', N'admin 1')
INSERT [dbo].[QL_NguoiDungNhomNguoiDung] ([TenDangNhap], [MaNhomNguoiDung], [GhiChu]) VALUES (N'staff1', N'1', N'staff1 1')
INSERT [dbo].[QL_NguoiDungNhomNguoiDung] ([TenDangNhap], [MaNhomNguoiDung], [GhiChu]) VALUES (N'staff1', N'2', N'staff1 2')
INSERT [dbo].[QL_NguoiDungNhomNguoiDung] ([TenDangNhap], [MaNhomNguoiDung], [GhiChu]) VALUES (N'staff2', N'1', N'staff2 1')
INSERT [dbo].[QL_NguoiDungNhomNguoiDung] ([TenDangNhap], [MaNhomNguoiDung], [GhiChu]) VALUES (N'staff2', N'2', N'staff2 2')
INSERT [dbo].[QL_NhomNguoiDung] ([MaNhom], [TenNhom], [GhiChu]) VALUES (N'1', N'admin', N'Admin')
INSERT [dbo].[QL_NhomNguoiDung] ([MaNhom], [TenNhom], [GhiChu]) VALUES (N'2', N'staff', N'staff')
INSERT [dbo].[QL_NhomNguoiDung] ([MaNhom], [TenNhom], [GhiChu]) VALUES (N'3', N'user', N'user')
USE [master]
GO
ALTER DATABASE [1.LTWNC] SET  READ_WRITE 
GO
