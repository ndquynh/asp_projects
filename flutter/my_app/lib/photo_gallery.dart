import 'dart:io';

import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  String title;

  MainPage({this.title}) : super();

  @override
  State<StatefulWidget> createState() {
    return new MainPageState();
  }
}

class MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new GridView.extent(
        maxCrossAxisExtent: 270,
        mainAxisSpacing: 5.0,
        crossAxisSpacing: 5.0,
        padding: EdgeInsets.all(5.0),
        children: _buildGridTiles(),
      ),
    );
  }
}

_buildGridTiles({tileNumber = 0}) {
  Directory imgFolder =
      new Directory(Platform.script.resolve("/").toFilePath(windows: false));

//  print("path: " + imgFolder.path);
//  print(Platform.script.pathSegments);
//  print("Platform.script.toFilePath(windows: true) " + Platform.script.toFilePath(windows: true));
//  print("Uri.base " + Uri.base.path);

  dynamic imgPaths = new List();
  imgFolder
      .list(recursive: false, followLinks: false)
      .listen((FileSystemEntity entity) {
    imgPaths.add(entity.path);
//    print(entity.path);
  });
  tileNumber = imgPaths.length;
  List<Stack> containers = new List<Stack>.generate(28, (index) {
    final img = 'images/image (${index + 1}).jpg';
    return new Stack(
      alignment: new Alignment(0.1, 1.0),
      children: <Widget>[
        new Container(
          child: new Image.asset(img,
            fit: BoxFit.cover,
            width: 270.0,
            height: 270.0,
          ),
        ),
        new Container(
          decoration: new BoxDecoration(
            color: Colors.grey[400],
          ),
          child: new Text(img),
        ),
      ],
    );
  });
  return containers;
}

class PhotoGallery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new MainPage(
      title: "League Of Legends",
    );
  }
}
