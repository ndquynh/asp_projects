import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:my_app/layout_basic.dart';
import 'package:my_app/photo_gallery.dart';

//void main() => runApp(new MyApp());
//void main() => runApp(new LayoutBasic());
//void main() => runApp(new PhotoGallery());

class RandomWords extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final wordPair = new WordPair.random();
    return new MaterialApp(
      title: "day la title",
      home: new RandomEnglishWords(),
    );
  }
}

class RandomEnglishWords extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new RandomEnglishWordState();
  }
}

class RandomEnglishWordState extends State<RandomEnglishWords> with WidgetsBindingObserver {
  AppLifecycleState _lastLifecycleState;

  @override
  Widget build(BuildContext context) {
    final wordPair = new WordPair.random();
    var _wordPairs = <WordPair>[];
    var _checkedWords = new Set<WordPair>();

//    return new Text(wordPair.asPascalCase, style: new TextStyle(fontFamily:"Times New Roman", fontSize: 111.2),);

    return createView(_wordPairs, _checkedWords);
  }

  void _navigateToCheckedWordsScreen(Set<WordPair> _checkedWords) {
    final pageRoute = new MaterialPageRoute(builder: (context) {
      final listTiles = _checkedWords.map((wordPair) {
        return new ListTile(
          title: new Text(
            wordPair.asPascalCase,
            style: new TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
          ),
        );
      });

      return new Scaffold(
        appBar: new AppBar(
          title: new Text("Checked words"),
        ),
        body: new ListView(
          children: listTiles.toList(),
        ),
      );
    });
    Navigator.of(context).push(pageRoute);
  }
}

void _showToast(BuildContext context) {
  final scaffold = Scaffold.of(context);
  scaffold.showSnackBar(
    SnackBar(
      content: const Text('Added to favorite'),
      action: SnackBarAction(
          label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
    ),
  );
}
createView(List<WordPair> wordPairs, Set<WordPair> checkedWords) {
  return new Scaffold(
    appBar: new AppBar(
      title: new Text("English word state app bar"),
      actions: <Widget>[
        new IconButton(
            icon: new Icon(Icons.chrome_reader_mode),
            onPressed: () {
//                _navigateToCheckedWordsScreen(checkedWords);
            })
      ],
    ),
    body: new ListView.builder(itemBuilder: (context, index) {
      if (index >= wordPairs.length) {
        wordPairs.addAll(generateWordPairs().take(10));
      }
      return _buildRowView(wordPairs[index], index, context, checkedWords);
    }),
  );
}

Widget _buildRowView(WordPair wordPair, int index, BuildContext context,
    Set<WordPair> _checkedWords) {
  Color color = index % 2 == 0 ? Colors.teal : Colors.pinkAccent;
  final isChecked = _checkedWords.contains(wordPair);

  return new ListTile(
      leading: new Icon(
        isChecked ? Icons.check_box : Icons.check_box_outline_blank,
        color: color,
        size: 32.0,
      ),
      title: new Text(
        wordPair.asPascalCase,
        style: new TextStyle(
            fontSize: 18, color: color, fontWeight: FontWeight.w800),
      ),
      onTap: () {
//        setState(() {
//          if (isChecked) {
//            _checkedWords.remove(wordPair);
//          } else {
//            _checkedWords.add(wordPair);
//          }
//        });
//          _showToast(context);
      });
}