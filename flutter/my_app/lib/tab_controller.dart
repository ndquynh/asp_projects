import 'package:flutter/material.dart';
import 'package:my_app/beautiful_list/beautiful_list.dart';
import 'package:my_app/children_tabs/tab1.dart';
import 'package:my_app/children_tabs/tab_horizontal_listview.dart';
import 'package:my_app/children_tabs/url_img.dart';
import 'package:my_app/children_tabs/tab_infinite_listview.dart';
import 'package:my_app/children_tabs/login_page.dart';
import 'package:my_app/drawer/drawer_demo.dart';
import 'package:my_app/layout_basic.dart';
import 'package:my_app/photo_gallery.dart';

class TabControllerDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DefaultTabController tabController = new DefaultTabController(
        length: 8,
        initialIndex: 7,
        child:
        new Scaffold(
          appBar: new AppBar(
            bottom: new TabBar(tabs: [
              new Tab(icon: new Icon(Icons.home), text: "Home",
              ),
              new Tab(icon: new Icon(Icons.grade), text: "Favorite"),
              new Tab(icon: new Icon(Icons.chat), text: "Chat"),
              new Tab(icon: new Icon(Icons.cloud_queue), text: "Cloud"),
              new Tab(icon: new Icon(Icons.photo), text: "Online Image"),
              new Tab(icon: new Icon(Icons.list), text: "Infinite list"),
              new Tab(icon: new Icon(Icons.line_style), text: "horizontal list"),
              new Tab(icon: new Icon(Icons.videogame_asset), text: "Apex"),
            ]),
          ),
          body: new TabBarView(children: [
            new Tab1(),
            new PhotoGallery(),
            new LayoutBasic(),
            new LoginPage(),
            new UrlImage(),
            new InfiniteListView(),
            new HorizontalListView(),
            new ApexListCharacter(),
          ]),
        )
    );

    return new MaterialApp(
      title: "><<<).>",
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text("multiple tabs"),
        ),
        body: new Container(
            child: tabController,
            decoration: new BoxDecoration(
              color: Colors.pinkAccent[500],
            )),
        drawer: buildDrawer(context),
      ),
    );
  }


}
