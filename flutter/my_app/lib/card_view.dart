import 'package:flutter/material.dart';

class CardView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    var card = new Card(
      child: new Column(
        children: <Widget>[
          new ListTile(
            leading: new Icon(Icons.account_box, color: Colors.blue, size: 32,),
            title: new Text("Nguyen Duc Quynh", style: new TextStyle(fontWeight: FontWeight.w400),),
            subtitle: new Text("Software developer"),
          ),
          new Divider(color: Colors.blue, indent: 16,),
          new ListTile(
            leading: new Icon(Icons.email, color: Colors.blue, size: 32,),
            title: new Text("nducquynh@tma.com.vn", style: new TextStyle(fontWeight: FontWeight.w400),),
          ),
          new Divider(color: Colors.blue, indent: 16,),
          new ListTile(
            leading: new Icon(Icons.phone, color: Colors.blue, size: 32,),
            title: new Text("+84-973.034.507", style: new TextStyle(fontWeight: FontWeight.w400),),
          ),

        ],
      ),
    );
    var sizedBox = new Container(
      child: SizedBox(
        child: card,
        height: 230.0,
      ),
    );
    return new MaterialApp(
      title: "",
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text("CardView"),
        ),
        body: new Container(
          child: new Center(
            child: sizedBox,
          ),
          decoration: new BoxDecoration(
            color: Colors.pinkAccent[500],
          )
        ),
      ),
    );
  }

}