class Flower {
  final String flowerName;
  final String description;
  final String imageUrl;

  Flower({this.flowerName, this.description, this.imageUrl});
}
