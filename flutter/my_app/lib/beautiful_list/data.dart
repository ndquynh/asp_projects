import 'package:my_app/beautiful_list/flower.dart';

var FlowerData = <Flower>[
  new Flower(
      flowerName: "BANGALORE",
      description: 'Smoke Launcher - Double Time - Rolling Thunder',
      imageUrl: "https://gamewith-en.akamaized.net/img/790a38c0c640c6d9898c26736b8f6db0.jpg"),
  new Flower(
      flowerName: "BLOODHOUND",
      description: 'Eye of the Allfather - Tracker - Beast of the Hunt',
      imageUrl: "https://gamewith-en.akamaized.net/img/7e768ea0157ce16629043742600adf60.jpg"),
  new Flower(
      flowerName: "CAUSTIC",
      description: 'Nox Gas Trap - Nox Vision - Nox Gas Grenade',
      imageUrl: "https://gamewith-en.akamaized.net/img/405ceb08e3271ba16c64c77ddff3bc58.jpg"),
  new Flower(
      flowerName: "GIBRALTAR",
      description: 'Dome of Protection - Gun Shield - Defensive Bombardment',
      imageUrl: "https://gamewith-en.akamaized.net/img/a30c7c64e2e6e86e52984570412fd9f7.jpg"),
  new Flower(
      flowerName: "LIFELINE",
      description: 'D.O.C. Heal Drone - Combat Medic - Care Package',
      imageUrl: "https://gamewith-en.akamaized.net/img/5b74499a5299581cdb5407f0b8317403.jpg"),
  new Flower(
      flowerName: "MIRAGE",
      description: 'Psyche Out - Encore! - Vanishing Act',
      imageUrl: "https://gamewith-en.akamaized.net/img/25f6684947db36b1835f6e91dfa7347c.jpg"),
  new Flower(
      flowerName: "PATHFINDER",
      description: 'Grappling Hook - Insider Knowledge - Zipline Gun',
      imageUrl: "https://gamewith-en.akamaized.net/img/a63405de1ce5fe320333449fdbaac235.jpg"),
  new Flower(
      flowerName: "WRAITH",
      description: 'Into the Void - Voices from the Void - Dimensional Rift',
      imageUrl: "https://gamewith-en.akamaized.net/img/42ba6e34631c4cb8bb8853149a4c7904.jpg"),
];
