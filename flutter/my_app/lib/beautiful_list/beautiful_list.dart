import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:my_app/beautiful_list/data.dart';
import 'package:my_app/beautiful_list/flower.dart';

class ApexListCharacter extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ApexListCharacterState();
  }
}

class ApexListCharacterState extends State<ApexListCharacter> {
  var selectedFlower = new Flower();

  @override
  Widget build(BuildContext context) {
    return new ListView(
      children: _buildListItems(),
    );
  }

  List<GestureDetector> _buildListItems() {
    var index = 0;

    return FlowerData.map((f) {
      var boxDecoration = new BoxDecoration(
        color: index % 2 == 0 ? Colors.green[300] : Colors.deepOrange[300],
      );

      if (selectedFlower == f) {
//        setState(() {
          boxDecoration = new BoxDecoration(color: Colors.deepPurple[300]);

//        });
      }
      var container = new Container(
        key: new Key("adasdas"),
        decoration: boxDecoration,
        child: new Row(
          children: <Widget>[
            new Container(
              margin: new EdgeInsets.all(10.0),
              child: new CachedNetworkImage(
                imageUrl: f.imageUrl,
                height: 300,
                fit: BoxFit.cover,
              ),
            ),
            new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: new Text(
                    f.flowerName,
                    style: new TextStyle(
                        fontSize: 24.0, fontWeight: FontWeight.bold),
                  ),
                ),
                new Container(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: new Text(
                    f.description,
                    style: new TextStyle(
                        fontSize: 14.0, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            )
          ],
        ),
      );
      index += 1;

      final gesture = new GestureDetector(
        child: container,
        onTap: () {
          setState(() {

          });
          selectedFlower = f;
          print("You tabbed ${f.flowerName}");
        },
      );

      return gesture;
    }).toList();
  }
}
