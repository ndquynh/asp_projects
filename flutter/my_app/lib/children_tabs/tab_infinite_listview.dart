import 'package:flutter/material.dart';

class InfiniteListView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new InfiniteListViewState();
  }
}

class InfiniteListViewState extends State<InfiniteListView> {
  var items = new List<String>.generate(20, (i) => "item number ${i + 1}");

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        child: new ListView.builder(
            itemCount: items.length + 1,
            itemBuilder: (context, index) {
              final widgetItem = (index == items.length)
                  ? new RaisedButton(
                      color: Colors.cyanAccent,
                      splashColor: Colors.deepOrange,
                      elevation: 4.0,
                      child: new Text("Load more ..."),
                      onPressed: () {
                        var newItems = new List<String>.generate(
                            20, (i) => "item number ${i + items.length}");
                        setState(() {
                          items.addAll(newItems);
                          print(items);
                        });
                      })
                  : new ListTile(
                      title: new Text(items[index]),
                    );
              return widgetItem;
            }),
      ),
    );
  }
}
