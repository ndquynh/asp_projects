import 'package:flutter/material.dart';

class Tab1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var card = new Card(
      child: new Column(
        children: <Widget>[
          new ListTile(
            leading: new Icon(Icons.account_box, color: Colors.blue, size: 32,),
            title: new Text("Nguyen Duc Quynh", style: new TextStyle(fontSize: 32, fontWeight: FontWeight.w700, fontFamily: 'Miss Fajardose'),),
            subtitle: new Text("Software developer", style: new TextStyle(fontSize: 30, fontWeight: FontWeight.w400, fontFamily: 'Sacramento'),),
          ),
          new Divider(color: Colors.blue, indent: 16,),
          new ListTile(
            leading: new Icon(Icons.email, color: Colors.blue, size: 32,),
            title: new Text("nducquynh@tma.com.vn", style: new TextStyle(fontSize: 30, fontWeight: FontWeight.w400, fontFamily: 'Poiret One'),),
          ),
          new Divider(color: Colors.blue, indent: 16,),
          new ListTile(
            leading: new Icon(Icons.phone, color: Colors.blue, size: 32,),
            title: new Text("+84-973.034.507", style: new TextStyle(fontWeight: FontWeight.w400),),
          ),

        ],
      ),
    );
    var sizedBox = new Container(
      child: SizedBox(
        child: card,
        height: 250.0,
      ),
    );

    return new Center(
      child: sizedBox,
    );
  }

}