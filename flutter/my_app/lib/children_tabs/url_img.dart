import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:cached_network_image/cached_network_image.dart';

class UrlImage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new UrlImageState();
  }
}

class UrlImageState extends State<UrlImage> {

  final imageUrls = [
    "https://3.bp.blogspot.com/-IdoRQuNfDPg/VoCnJTU0HrI/AAAAAAAAL_k/Rj3rceZ7nSo/s1600/6gUVE.jpg",
    "https://afamilycdn.com/k:thumb_w/600/D7prXinbUB7s5ppWxBGqtwKrIZ1zKF/Image/2013/03/20130306afamilyPASongjoongkimangniceguydenvietnam-(9)-c3c5e/song-oong-ki-mang-nice-guy-den-viet-nam.jpg",
    "https://afamilycdn.com/k:thumb_w/600/D7prXinbUB7s5ppWxBGqtwKrIZ1zKF/Image/2013/03/20130306afamilyPASongjoongkimangniceguydenvietnam-(10)-c3c5e/song-oong-ki-mang-nice-guy-den-viet-nam.jpg",
    "https://afamilycdn.com/k:thumb_w/600/D7prXinbUB7s5ppWxBGqtwKrIZ1zKF/Image/2013/03/20130306afamilyPASongjoongkimangniceguydenvietnam-(3)-c3c5e/song-oong-ki-mang-nice-guy-den-viet-nam.jpg",
    "https://afamilycdn.com/k:thumb_w/600/D7prXinbUB7s5ppWxBGqtwKrIZ1zKF/Image/2013/03/20130306afamilyPASongjoongkimangniceguydenvietnam-(4)-c3c5e/song-oong-ki-mang-nice-guy-den-viet-nam.jpg",
    "https://afamilycdn.com/k:thumb_w/600/D7prXinbUB7s5ppWxBGqtwKrIZ1zKF/Image/2013/03/20130306afamilyPASongjoongkimangniceguydenvietnam-(5)-c3c5e/song-oong-ki-mang-nice-guy-den-viet-nam.jpg",
    "https://afamilycdn.com/k:thumb_w/600/D7prXinbUB7s5ppWxBGqtwKrIZ1zKF/Image/2013/03/20130306afamilyPASongjoongkimangniceguydenvietnam-(8)-c3c5e/song-oong-ki-mang-nice-guy-den-viet-nam.jpg",
    "https://afamilycdn.com/k:thumb_w/600/D7prXinbUB7s5ppWxBGqtwKrIZ1zKF/Image/2013/03/20130306afamilyPASongjoongkimangniceguydenvietnam-(7)-c3c5e/song-oong-ki-mang-nice-guy-den-viet-nam.jpg",

  ];
  var selectedIdx = 0;

  @override
  Widget build(BuildContext context) {
    final cachedImage = new CachedNetworkImage(
      imageUrl: imageUrls[selectedIdx],
      placeholder: new CircularProgressIndicator(),
      height: 600.0,
    );

    var controller = new TextEditingController(text: selectedIdx.toString());
    var monitor = new TextFormField(
//      initialValue: selectedIdx.toString(),
      controller: controller,
    );

    return new Center(
        child: new Container(
      margin: EdgeInsets.all(10.0),
      child: new Column(
        children: <Widget>[
          monitor,
          cachedImage,
          new Row(
            children: <Widget>[
              new Expanded(
                  child: new RaisedButton(
                onPressed: () {
                  setState(() {
                    selectedIdx = selectedIdx - 1 < 0 ? imageUrls.length - 1 : --selectedIdx;
                    controller.text = selectedIdx.toString();
                  });
                  print(" < ${selectedIdx}");
                },
                child: ListTile(
                  leading: Icon(Icons.navigate_before),
                  title: new Text("Back"),
                ),
              )),
              new Expanded(
                  child: new RaisedButton(
                onPressed: () {
                  setState(() {
                    selectedIdx = selectedIdx + 1 >= imageUrls.length ? 0 : ++selectedIdx;
                    controller.text = selectedIdx.toString();
                    print(" > ${selectedIdx}");
                  });
                },
                child: ListTile(
                  leading: Icon(Icons.navigate_next),
                  title: new Text("Next"),
                ),
              ))
            ],
          )
        ],
      ),
    ));
  }
}
