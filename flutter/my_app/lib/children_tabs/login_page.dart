import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final TextField _txtUsername = new TextField(
      decoration: new InputDecoration(
        hintText: 'Enter your username',
        hintStyle: new TextStyle(
//              fontFamily: "Miss Fajardose",
            fontSize: 25,
            fontWeight: FontWeight.w500),
        contentPadding: EdgeInsets.all(10.0),
        border: InputBorder.none,
      ),
      style: new TextStyle(
//              fontFamily: "Miss Fajardose",
          fontSize: 25,
          color: Colors.grey,
          fontWeight: FontWeight.w500),
      keyboardAppearance: Brightness.dark,
      autocorrect: false,
      keyboardType: TextInputType.text,
    );
    final TextField _txtEmail = new TextField(
      decoration: new InputDecoration(
        hintText: 'Enter your email',
        hintStyle: new TextStyle(
//              fontFamily: "Miss Fajardose",
            fontSize: 25,
            fontWeight: FontWeight.w500),
        contentPadding: EdgeInsets.all(10.0),
        border: InputBorder.none,
      ),
      style: new TextStyle(
//              fontFamily: "Miss Fajardose",
          fontSize: 25,
          color: Colors.grey,
          fontWeight: FontWeight.w500),
      keyboardAppearance: Brightness.dark,
      autocorrect: false,
      keyboardType: TextInputType.emailAddress,
    );
    final TextField _txtPassword = new TextField(
      decoration: new InputDecoration(
        hintText: 'Enter your password',
        hintStyle: new TextStyle(
//              fontFamily: "Miss Fajardose",
            fontSize: 25,
            fontWeight: FontWeight.w500),
        contentPadding: EdgeInsets.all(10.0),
        border: InputBorder.none,
      ),
      style: new TextStyle(
//              fontFamily: "Miss Fajardose",
          color: Colors.grey,
          fontSize: 25,
          fontWeight: FontWeight.w500),
      keyboardAppearance: Brightness.dark,
      autocorrect: false,
      obscureText: true,
      keyboardType: TextInputType.text,
    );
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Container(
          margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
          decoration: new BoxDecoration(
              color: new Color.fromARGB(255, 240, 240, 240),
              border: new Border.all(width: 1.2, color: Colors.black12),
              borderRadius: BorderRadius.all(Radius.circular(6.0))),
          child: _txtUsername,
        ),
        new Container(
          margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
          decoration: new BoxDecoration(
            color: new Color.fromARGB(255, 240, 240, 240),
            border: new Border.all(width: 1.2, color: Colors.black12),
            borderRadius: BorderRadius.all(
              Radius.circular(6.0),
            ),
          ),
          child: _txtEmail,
        ),
        new Container(
          margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
          decoration: new BoxDecoration(
              color: new Color.fromARGB(255, 240, 240, 240),
              border: new Border.all(width: 1.2, color: Colors.black12),
              borderRadius: BorderRadius.all(Radius.circular(6.0))),
          child: _txtPassword,
        ),
        new Container(
          margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
          child: new Row(
            children: <Widget>[
              new Expanded(
                  child: new RaisedButton(
                onPressed: () {},
                color: Colors.blue,
                textColor: Colors.white,
                child: new Text("Login"),
              )),
              new Expanded(
                child: new Image.asset("images/ratio.png"),
              ),
              new Expanded(
                child: new Image.asset("images/1.5x/ratio.png"),
              ),
              new Expanded(
                child: new Image.asset("images/2.0x/ratio.png"),
              ),
              new Expanded(
                child: new Image.asset("images/ratio.png"),
              )
            ],
          ),
        ),
      ],
    );
  }
}
