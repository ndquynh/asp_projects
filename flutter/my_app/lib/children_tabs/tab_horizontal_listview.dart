import 'package:flutter/material.dart';

class HorizontalListView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new HorizontalListViewState();
  }
}

class HorizontalListViewState extends State<HorizontalListView> {
  var items = new List<String>.generate(20, (i) => "item number ${i + 1}");

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return new Scaffold(
      body: new Container(
        child: ListView(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          children: <Widget>[
            new Container(
              width: size.width,
              height: size.height,
              color: Colors.deepOrange,
            ),
            new Container(
              width: size.width,
              height: size.height,
              color: Colors.green,
            ),
            new Container(
              width: size.width,
              height: size.height,
              color: Colors.yellow,
            ),
            new Container(
              width: size.width,
              height: size.height,
              color: Colors.redAccent,
            )
          ],
        ),
      ),
    );
  }
}
