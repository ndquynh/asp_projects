import 'package:flutter/material.dart';

Drawer buildDrawer(context) {
  return new Drawer(
      child: new Container(
    child: new ListView(
      padding: EdgeInsetsDirectional.zero,
      children: <Widget>[
        new DrawerHeader(
          child: new Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Image.asset(
                  'images/profile.png',
                  width: 70,
                  height: 70,
                  fit: BoxFit.cover,
                ),
                new Text(
                  "Nguyen Duc Quynh",
//                "Nguyễn Đức Quỳnh",
                  style: new TextStyle(
//                    fontFamily: "Sacramento",
//                    fontFamily: "Miss Fajardose",
                      fontFamily: "Poiret One",
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
                new Text(
                  "Software Developer",
//                "Nguyễn Đức Quỳnh",
                  style: new TextStyle(
//                    fontFamily: "Sacramento",
                      fontFamily: "Poiret One",
//                    fontFamily: "Miss Fajardose",
                      fontSize: 20,
                      fontWeight: FontWeight.w300,
                      color: Colors.white),
                )
              ],
            ),
          ),
          decoration: new BoxDecoration(
            color: Colors.blue,

          ),
          curve: new ElasticInCurve(),
        ),
        new Container(
          child: new Column(
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.home),
                title: new Text("Home"),
                onTap: () {
                  Navigator.pop(context);
                },
              ),ListTile(
                leading: Icon(Icons.favorite),
                title: new Text("Favorite"),
                onTap: () {
                  Navigator.pop(context);
                },
              ),ListTile(
                leading: Icon(Icons.chat),
                title: new Text("Chat"),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              new Divider(
                color: Colors.black45,
                indent: 16.0,
                height: 8.0,
              ),
              ListTile(
                leading: Icon(Icons.cloud_queue),
                title: new Text("Cloud"),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        )
        
      ],
    ),
  ));
}
