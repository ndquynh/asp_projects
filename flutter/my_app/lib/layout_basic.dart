import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LayoutBasic extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = new Container(
      padding: const EdgeInsets.all(10.0),
      child: new Row(
        children: <Widget>[
          new Expanded(
              child: new Column(
            children: <Widget>[
              new Container(
                child: new Text("Programing excersice",
                    style: new TextStyle(fontWeight: FontWeight.bold)),
                padding: new EdgeInsets.all(10),
              ),
              new Text(
                  "This is some scrap shit text. "
                  "I dont really want to think what it is. And should I think of a better content to fill in? "
                  "That would wait my time, worthlessly",
                  style:
                      new TextStyle(color: Colors.grey[600], fontSize: 16.0)),
            ],
            crossAxisAlignment: CrossAxisAlignment.start,
          )),
          new Icon(
            Icons.favorite,
            color: Colors.red,
          ),
          new Text(
            " 1.7 Mil",
            style: new TextStyle(color: Colors.grey[800]),
          ),
        ],
      ),
    );

    _buildButton(IconData icon, String buttonTitle, callback) {
      final Color tintColor = Colors.blue;
      return new Expanded(
        child: new Column(
          key: Key(buttonTitle),
          children: <Widget>[
            new IconButton(
                icon: new Icon(icon, color: tintColor, size: 32),
                onPressed: callback),
            new Text(
              buttonTitle,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                  color: tintColor),
            )
          ],
        ),
      );
    }

    Widget fourButtonsSection(BuildContext _context) {
      return new Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildButton(Icons.home, "Home", () {
            print("clicked Home");
            _showToast(_context, "home");
          }),
          _buildButton(Icons.arrow_back, "Back", () {
            print("clicked share");
            _showToast(_context, "clicked Back");
          }),
          _buildButton(Icons.arrow_forward, "Next", () {
            print("clicked share");
            _showToast(_context, "arrow_forward");
          }),
          _buildButton(Icons.share, "Share", () {
            _showToast(_context, "share");
          })
        ],
      );
    }

    Widget textContent = new Container(
      padding: new EdgeInsets.all(20.0),
      child: new Text(
        '''Trong bài trước chúng ta đã bố trí 1 Image và các Text xung quanh Image. Bài này tiếp tục chia sẻ với các bạn cách bố trí các layout trong Flutter:''' +
            "Chúng ta sẽ tiếp tục tạo ra 4 button bố trí đều nằm bên dưới Text, mỗi nút gồm có tiêu đề và Icon của nút được bố trí theo hàng cột. Việc tạo ra các nút bấm được viết thành các function/thuộc tính riêng với các params truyền vào là buttonTitle và Icon. Bên dưới bộ 4 nút bấm là 1 Text dài, chứa nội dung dưới dạng mô tả hay paragraph nào đó. Để đưa 1 string dài vào bên trong một Text, ta sử dụng 3 dấu nháy đơn (three single quotes) ở vị trí đầu và cuối string đó. Tải Project: Nếu việc thực hành theo hướng dẫn không diễn ra suôn sẻ như mong muốn. Bạn cũng có thể tải xuống PROJECT THAM KHẢO ở link bên dưới!",
        style: new TextStyle(color: Colors.grey[400]),
      ),
    );

    return new Scaffold(body: Builder(builder: (BuildContext context) {
      return new ListView(
        children: <Widget>[
//              titleSection,
          new Container(
            padding: new EdgeInsets.all(10.0),
            child: new Image.asset(
              "images/eagle.png",
              fit: BoxFit.cover,
            ),
          ),
          titleSection,
          fourButtonsSection(context),
          textContent,
        ],
      );
    }));
  }
}

void _showToast(BuildContext context, String s) {
  final scaffold = Scaffold.of(context);
  scaffold.showSnackBar(
    SnackBar(
      content: new Text(s),
      action: SnackBarAction(
          label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
    ),
  );
}
