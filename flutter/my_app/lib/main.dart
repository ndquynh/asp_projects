import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:my_app/drawer/drawer_demo.dart';
import 'package:my_app/card_view.dart';
import 'package:my_app/layout_basic.dart';
//import 'package:my_app/photo_gallery.dart';
import 'package:my_app/random_words.dart';
import 'package:my_app/tab_controller.dart';

void main() => runApp(new MyApp());
//void main() => runApp(new LayoutBasic());
//void main() => runApp(new TabControllerDemo());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return new MaterialApp(
      title: "Drawer in Flutter",
      home: new TabControllerDemo(),
    );
  }
}

class MainScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new StateObject();
  }
}

class StateObject extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("samples"),
      ),
      body: new Builder(builder: (BuildContext context) {
        return new Container(
          child: new Row(
            children: <Widget>[
              new FlatButton(
                  onPressed: () {
                    _showToast(context, "english words samples");
                    Navigator.of(context).push(_buildEnglishPageRoute(context));
                  },
                  child: new Text("english words samples")),
              new FlatButton(
                  onPressed: () {
                    _showToast(context, "basic layout");
                  },
                  child: new Text("basic layout")),
              new FlatButton(
                  onPressed: () {
                    _showToast(context, "photo galery");
                  },
                  child: new Text("photo gallery"))
            ],
          ),
        );
      }),
    );
  }
  Route<Object> _buildEnglishPageRoute(_context) {
    final pageRoute = new MaterialPageRoute(builder: (BuildContext context) {
//        return new RandomWords();
      return createView(<WordPair>[], new Set<WordPair>());
    });
    Navigator.of(context).push(pageRoute);
  }
}

void _showToast(BuildContext context, String s) {
  final scaffold = Scaffold.of(context);
  scaffold.showSnackBar(
    SnackBar(
      content: new Text(s),
      action: SnackBarAction(
          label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
    ),
  );
}
