﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfragisticsProductApp.Core
{
    class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public bool inStock { get; set; }
    }
}
