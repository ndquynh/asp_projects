﻿using ProductApp.Asp.Custom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProductApp.Asp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            
            return View();
        }


        [ActionName("what-us")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View("About");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Having a trouble? Send us a message!";

            return View();
        }

        [MyFilterAttribute]
        [HttpPost]
        public ActionResult Contact(String txaMessage)
        {
            ViewBag.Message = "Thanks! We recieved your message: " + txaMessage;

            return View();
        }
    }
}