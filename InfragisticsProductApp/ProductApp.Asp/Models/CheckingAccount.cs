﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProductApp.Asp.Models
{
    public class CheckingAccount
    {
        public int Id { get; set; }

        [Required]
        [StringLength(14, ErrorMessage = "The account number length must be 13 or 14 numerics", MinimumLength = 13)]
        [Display(Name = "Account #")]
        public string AccountNumber { get; set; }

        [Required]
        [Display(Name = "Fist Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        public string Name { get {
                return string.Format("{0} {1}", this.FirstName, LastName);
            } }

        [Required]
        [RegularExpression(@"\d+", ErrorMessage = "The balance must be a number")]
        [Range(100, Double.MaxValue, ErrorMessage = "The balance must be $100 at lest")]
        [DataType(DataType.Currency)]
        public decimal Balance { get { return Balance/1; } set { Balance = value; } }
    }
}