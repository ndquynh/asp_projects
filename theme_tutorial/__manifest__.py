{
  'name':'Tutorial theme',
  'description': 'A description for my theme: NDQ\'s the first Odoo theme.',
  'version':'1.0',
  'author':'NDQ',

  'data': [
	'views/layout.xml',
    'views/pages.xml',
    'views/assets.xml',
    'views/snippets.xml',
    'views/options.xml',
  ],
  'category': 'Theme/Creative',
  'depends': ['website']
}
